using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BATUSystems.AdminPanel.API.Helpers;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.WebSockets;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Npgsql;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Swashbuckle.AspNetCore.Swagger;

namespace BATUSystems.AdminPanel.Panel {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {

            services.AddAutoMapper ();

            services.AddMvc ()
                .AddJsonOptions (
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                )
                .SetCompatibilityVersion (CompatibilityVersion.Version_2_1);

            // services.AddAuthorization (options => {
            //     options.AddPolicy ("AdminOnly", policy => policy.RequireClaim(ClaimTypes.Role, "Admin"));
            // });

            services.AddSwaggerGen (c => {
                c.SwaggerDoc (
                    "v1",
                    new Info {
                        Title = "Bakalie Admin Api",
                            Version = "v1",
                            Description = "A simple example ASP.NET Core Web API",
                            TermsOfService = "None",
                            Contact = new Contact {
                                Name = "Paweł Barcicki",
                                    Email = string.Empty,
                                    Url = "https://twitter.com/spboyer"
                            },
                            License = new License {
                                Name = "BATUSystems License",
                                    Url = "https://example.com/license"
                            }
                    });
            });

            services.AddDbContext<BakalieDevContext> (options => {
                options.UseNpgsql (Configuration.GetConnectionString ("BakalieDevDatabase"));
            });

            services.AddAuthentication ("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler> ("BasicAuthentication", null);

            // configure DI for application services
            services.AddScoped<IAdminService, AdminService> ();
            services.AddScoped<IMessageService, MessageService> ();

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles (configuration => {
                configuration.RootPath = "AdminPanel/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {

            loggerFactory.AddConsole (Configuration.GetSection ("Logging"));
            loggerFactory.AddDebug ();

            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            } else {
                app.UseExceptionHandler ("/Error");
                app.UseHsts ();
            }

            var webSocketOptions = new WebSocketOptions () {
                KeepAliveInterval = TimeSpan.FromSeconds (120),
                ReceiveBufferSize = 4 * 1024
            };
            app.UseWebSockets (webSocketOptions);

            app.Map ("/chat", (_app) => _app.UseMiddleware<ChatWebSocketMiddleware> ());
            app.Map ("/notifications", (_app) => _app.UseMiddleware<NotificationWebSocketMiddleware> ());

            //app.UseHttpsRedirection ();
            app.UseStaticFiles ();
            app.UseSpaStaticFiles ();

            app.UseCors (x => x
                .AllowAnyOrigin ()
                .AllowAnyMethod ()
                .AllowAnyHeader ()
                .AllowCredentials ());

            app.UseAuthentication ();

            app.UseSwagger ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Bakalie Admin Api :D");
            });

            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa (spa => {
                spa.Options.SourcePath = "AdminPanel";

                if (env.IsDevelopment ()) {
                    spa.UseAngularCliServer (npmScript: "start");
                }
            });

        }
    }
}