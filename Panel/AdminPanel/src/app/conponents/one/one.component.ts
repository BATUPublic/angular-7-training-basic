import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.less']
})
export class OneComponent implements OnInit {
  public active = 'inbox';

  @Output()
  public current: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  setActive(value: string) {
    this.active = value;
    this.current.emit(value);
  }

  ngOnInit() {}
}
