import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.less']
})
export class TwoComponent implements OnInit {
  @Input()
  public filter: string;

  constructor() {}

  ngOnInit() {}
}
