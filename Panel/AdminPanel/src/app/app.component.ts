import { Component } from '@angular/core';
import { MenuService } from './services/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'AdminPanel';
  private menuService: MenuService;

  constructor(_menuService: MenuService) {
    this.menuService = _menuService;
  }

  getClasses() {
    return {
      'col-md-9': this.getMenuState(),
      'col-lg-10': this.getMenuState(),
      'col-md-12': !this.getMenuState(),
      'col-lg-12': !this.getMenuState()
    };
  }

  getMenuState(): boolean {
    return this.menuService.getOpen();
  }
}
