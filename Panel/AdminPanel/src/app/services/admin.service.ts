import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../models/admin';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private baseUrl = 'api/admin';

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL')
    private hostUrl: string
  ) {}

  get(): Observable<Admin[]> {
    return this.http.get<Admin[]>(this.hostUrl + this.baseUrl);
  }

  one(id: number): Observable<Admin> {
    return this.http.get<Admin>(`${this.hostUrl}${this.baseUrl}/${id}`);
  }

  update(itemToUpdate: Admin): Observable<null> {
    return this.http.put<null>(`${this.hostUrl + this.baseUrl}/${itemToUpdate.id}`, itemToUpdate);
  }
}
