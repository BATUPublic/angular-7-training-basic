import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private isOpen = true;

  private OpenChange: EventEmitter<boolean> = new EventEmitter();

  public get openChange(): EventEmitter<boolean> {
    return this.OpenChange;
  }

  constructor() {}

  toggle(): void {
    this.isOpen = !this.isOpen;
    this.OpenChange.emit(this.isOpen);
  }

  getOpen(): boolean {
    return this.isOpen;
  }
}
