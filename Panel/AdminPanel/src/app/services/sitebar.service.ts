import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SitebarService {
  private _current = true;
  private _change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  public getChange(): EventEmitter<boolean> {
    return this._change;
  }
  public get change(): EventEmitter<boolean> {
    return this._change;
  }

  public set current(value: boolean) {
    this._current = value;
    this._change.emit(value);
  }

  public get current(): boolean {
    return this._current;
  }

  toggleValue() {
    this.current = !this.current;
  }
}
