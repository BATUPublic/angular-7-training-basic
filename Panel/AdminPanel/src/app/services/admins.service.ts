import { Observable } from 'rxjs';
import { Admin } from 'src/app/models/admin';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminsService {
  constructor(private http: HttpClient) {}

  add(admin: Admin): Observable<Admin> {
    console.log('AdminService.add');
    return this.http.post<Admin>('/api/admin', admin);
  }

  get(id: number): Observable<Admin> {
    console.log('AdminsService.get');
    return this.http.get<Admin>(`/api/admin/${id}`);
  }

  list(): Observable<Admin[]> {
    console.log('AdminsService.list');

    let tmp = this.http.get<Admin[]>('/api/admin');

    // tmp.toPromise().then(x => console.log('x', x));
    // tmp.toPromise().then(item => Object.assign(new Admin(), item));

    return tmp;
  }

  update(admin: Admin): Observable<Admin> {
    console.log('AdminService.update');
    return this.http.post<Admin>(`/api/admin/${admin.id}`, admin);
  }
}
