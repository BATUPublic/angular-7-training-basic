import { TestBed } from '@angular/core/testing';

import { SitebarService } from './sitebar.service';

describe('SitebarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SitebarService = TestBed.get(SitebarService);
    expect(service).toBeTruthy();
  });
});
