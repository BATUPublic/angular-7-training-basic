import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopComponent } from './components/top/top.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OneComponent } from './conponents/one/one.component';
import { TwoComponent } from './conponents/two/two.component';
import { AdminListComponent } from './components/admin-list/admin-list.component';
import { AdminCreateComponent } from './components/admin-create/admin-create.component';
import { AdminEditComponent } from './components/admin-edit/admin-edit.component';

@NgModule({
  declarations: [AppComponent, SidebarComponent, TopComponent, DashboardComponent, OneComponent, TwoComponent, AdminListComponent, AdminCreateComponent, AdminEditComponent],
  imports: [BrowserModule, HttpClientModule, ReactiveFormsModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
