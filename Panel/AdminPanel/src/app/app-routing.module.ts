import { AdminListComponent } from './components/admin-list/admin-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {AdminCreateComponent} from "./components/admin-create/admin-create.component";
import {AdminEditComponent} from "./components/admin-edit/admin-edit.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DashboardComponent
  },
  {
    path: 'admin',
    component: AdminListComponent
  },
  {
    path: 'admin/create',
    component: AdminCreateComponent
  },
  {
    // path: 'admin/:id-:name',
    path: 'admin/:id',
    component: AdminEditComponent
  },
  {
    path: '**',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
