import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  name: string;
  currentTab: string;

  constructor() {}

  send(_name) {
    console.log('name', _name);
  }

  ngOnInit() {
    this.name = 'Angular 7';
  }
}
