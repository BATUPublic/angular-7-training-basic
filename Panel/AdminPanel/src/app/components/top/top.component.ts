import { SitebarService } from './../../services/sitebar.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.less']
})
export class TopComponent implements OnInit {
  constructor(private serviceSitebar: SitebarService) {}

  toggleMenu() {
    this.serviceSitebar.toggleValue();
  }

  ngOnInit() {}
}
