import { Admin } from 'src/app/models/admin';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminsService } from 'src/app/services/admins.service';

@Component({
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.less']
})
export class AdminListComponent implements OnInit, OnDestroy {
  private sub: any;
  private _adminsList: Admin[];
  public get adminsList(): Admin[] {
    return this._adminsList;
  }
  constructor(private adminsService: AdminsService) {}

  ngOnInit() {
    this.sub = this.adminsService.list().subscribe(admins => (this._adminsList = admins));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
