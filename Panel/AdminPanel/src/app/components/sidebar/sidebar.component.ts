import { Component, OnInit, OnDestroy } from '@angular/core';
import { SitebarService } from 'src/app/services/sitebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit, OnDestroy {
  private sub: any;
  isOpen: boolean;
  constructor(private servceSiterbar: SitebarService) {}

  ngOnInit(): void {
    this.sub = this.servceSiterbar.getChange().subscribe(x => (this.isOpen = x));

    this.isOpen = this.servceSiterbar.current;
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  show(text: string): void {
    console.log('test', text);
  }

  // getMenuState(): boolean {
  //   return this.menuService.getOpen();
  // }

  // ngOnInit() {
  //   this.sub = this.menuService.openChange.subscribe(x => console.log('zmiana', x));
  // }

  // ngOnDestroy(): void {
  //   this.sub.unsubscribe();
  // }
}
