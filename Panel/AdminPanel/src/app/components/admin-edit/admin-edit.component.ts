import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {Admin} from "../../models/admin";
import {AdminsService} from "../../services/admins.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.less']
})
export class AdminEditComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  private admin: Admin;
  public adminForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route : ActivatedRoute,
    private adminsService: AdminsService) { }

  ngOnInit() {
    this.adminForm = this.formBuilder.group({
      username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.pattern('^[a-z]+$')]),
      firstname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      surename: new FormControl('', [Validators.required, Validators.minLength(3)])
    });


    this.sub = this.route.params.subscribe(params => {
      // console.log(params);
      if (params['id']) {
        const id: number = parseInt(params['id'], 0);
        this.adminsService.get(id).subscribe(admin => {
          this.admin = admin;
          for (const key of Object.keys(this.adminForm.value)){
            if (this.admin.hasOwnProperty(key)) {
              this.adminForm.get(key).setValue(this.admin[key]);
            }
          }
          // this.adminForm.setValue(admin);
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  onSubmit() {
    if (this.adminForm.invalid) {
      return;
    }

    const _admin = Object.assign(this.admin, this.adminForm.value);
    console.log(_admin);
    this.adminsService.update(_admin).subscribe(admin => (this.admin = admin));
  }

}
