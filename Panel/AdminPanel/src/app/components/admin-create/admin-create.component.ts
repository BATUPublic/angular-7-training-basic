import {Component, OnInit, ViewChild} from '@angular/core';
import {Admin} from "../../models/admin";
import {NgForm} from "@angular/forms";
import {AdminsService} from "../../services/admins.service";

@Component({
  selector: 'app-admin-create',
  templateUrl: './admin-create.component.html',
  styleUrls: ['./admin-create.component.less']
})
export class AdminCreateComponent implements OnInit {

  public  admin:Admin = new Admin();
  @ViewChild('adminForm')
  private adminForm: NgForm;

  constructor(private adminsService: AdminsService) { }

  ngOnInit() {
  }

  doSubmit() {
    if (this.adminForm.invalid) {
      return;
    }

    this.adminsService.add(this.admin).subscribe();
  }

  isValid() {
    return {
      'btn-success': this.adminForm.valid,
      'btn-danger': this.adminForm.invalid
    }
  }
}
