export class Admin {
  id: number;
  username: string;
  firstname: string;
  surname: string;
  email: string;
  avatar: string;
  lastLogin: Date;
  groups: Array<string>;

  public get displayname():string {
    return this.firstname + ' ' + this.surname;
  }
}
