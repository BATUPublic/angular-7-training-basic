﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BATUSystems.AdminPanel.Model.Migrations {
    public partial class m2 : Migration {
        protected override void Up (MigrationBuilder migrationBuilder) {
            migrationBuilder.AddColumn<DateTime> (
                name: "last_login",
                table: "admin",
                nullable : true
            );
        }

        protected override void Down (MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn (
                name: "last_login",
                table: "admin"
            );
        }
    }
}