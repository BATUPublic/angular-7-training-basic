﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BATUSystems.AdminPanel.Model.Migrations
{
    public partial class messages2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "content",
                table: "message",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "important",
                table: "message",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "read",
                table: "message",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "sent",
                table: "message",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "stared",
                table: "message",
                nullable: false,
                defaultValueSql: "false");

            migrationBuilder.AddColumn<string>(
                name: "topic",
                table: "message",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "labels",
                table: "message",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "deleted",
                table: "message",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "content",
                table: "message");

            migrationBuilder.DropColumn(
                name: "important",
                table: "message");

            migrationBuilder.DropColumn(
                name: "read",
                table: "message");

            migrationBuilder.DropColumn(
                name: "sent",
                table: "message");

            migrationBuilder.DropColumn(
                name: "stared",
                table: "message");

            migrationBuilder.DropColumn(
                name: "topic",
                table: "message");

            migrationBuilder.DropColumn(
                name: "labels",
                table: "message");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "message");
        }
    }
}
