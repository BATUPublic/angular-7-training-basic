﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BATUSystems.AdminPanel.Model.Migrations
{
    public partial class messages3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "important",
                table: "message",
                nullable: false,
                defaultValueSql: "false",
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "important",
                table: "message",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "false");
        }
    }
}
