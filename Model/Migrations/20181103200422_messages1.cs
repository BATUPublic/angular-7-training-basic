﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BATUSystems.AdminPanel.Model.Migrations {
    public partial class messages1 : Migration {
        protected override void Up (MigrationBuilder migrationBuilder) {
            migrationBuilder.CreateTable (
                name: "message",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        from_id = table.Column<int> (nullable: false),
                        to_id = table.Column<int> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_message", x => x.id);
                    table.ForeignKey (
                        name: "FK_message_admin_from_id",
                        column : x => x.from_id,
                        principalTable: "admin",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Cascade);
                    table.ForeignKey (
                        name: "FK_message_admin_to_id",
                        column : x => x.to_id,
                        principalTable: "admin",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex (
                name: "IX_message_from_id",
                table: "message",
                column: "from_id");

            migrationBuilder.CreateIndex (
                name: "IX_message_to_id",
                table: "message",
                column: "to_id");
        }

        protected override void Down (MigrationBuilder migrationBuilder) {
            migrationBuilder.DropTable (
                name: "message");
        }
    }
}