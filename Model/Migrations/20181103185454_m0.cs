﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BATUSystems.AdminPanel.Model.Migrations {
    public partial class m0 : Migration {
        protected override void Up (MigrationBuilder migrationBuilder) {
            migrationBuilder.AlterDatabase ()
                .Annotation ("Npgsql:PostgresExtension:plv8", "'plv8', '', ''");

            migrationBuilder.CreateSequence (
                name: "product_id_seq");

            migrationBuilder.CreateSequence (
                name: "product_id_seq1");

            migrationBuilder.CreateTable (
                name: "admin",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        username = table.Column<string> (maxLength: 128, nullable: false),
                        password = table.Column<string> (maxLength: 32, nullable: false),
                        firstname = table.Column<string> (maxLength: 128, nullable: false),
                        lastname = table.Column<string> (maxLength: 256, nullable: false),
                        email = table.Column<string> (maxLength: 256, nullable: false),
                        hash = table.Column<string> (maxLength: 32, nullable: true),
                        group = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'[]'::jsonb"),
                        avatar = table.Column<string> (maxLength: 128, nullable: true),
                        last_login = table.Column<DateTime> (type: "timestamp with time zone", nullable : false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_admin", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "advertise",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        place = table.Column<string> (maxLength: 50, nullable: false),
                        category = table.Column<string> (nullable: true),
                        content = table.Column<string> (nullable: false),
                        date_active = table.Column<DateTime> (type: "date", nullable : false),
                        active = table.Column<bool> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_advertise", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "api",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 128, nullable: false),
                        code = table.Column<string> (maxLength: 128, nullable: false),
                        url = table.Column<string> (maxLength: 256, nullable: false),
                        orderby = table.Column<int> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_api", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "article",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        content = table.Column<string> (nullable: false),
                        rewrite = table.Column<string> (maxLength: 256, nullable: false),
                        category_id = table.Column<int> (nullable: false, defaultValueSql: "1"),
                        instrumental = table.Column<string> (maxLength: 256, nullable: true),
                        images = table.Column<string> (nullable: true),
                        tags = table.Column<string> (nullable: true),
                        type = table.Column<string> (maxLength: 64, nullable: false, defaultValueSql: "'info'::character varying"),
                        countries = table.Column<string> (nullable: true),
                        slider1 = table.Column<string> (nullable: true),
                        slider2 = table.Column<string> (nullable: true),
                        categories = table.Column<string> (type: "json", nullable : true),
                        author_id = table.Column<int> (nullable: true),
                        verify = table.Column<bool> (nullable: false),
                        verification_date = table.Column<DateTime> (type: "date", nullable : true),
                        _images_count = table.Column<int> (nullable: true),
                        _tags_count = table.Column<int> (nullable: true),
                        _countries_count = table.Column<int> (nullable: true),
                        publication_date = table.Column<DateTime> (type: "date", nullable : false, defaultValueSql: "(now())::timestamp without time zone"),
                        img_transfer = table.Column<bool> (nullable: true),
                        _first_image_url = table.Column<string> (nullable: true),
                        shows_count = table.Column<int> (nullable: true, defaultValueSql: "0"),
                        brief = table.Column<string> (nullable: true),
                        firstimageurl = table.Column<string> (maxLength: 256, nullable: true),
                        metadescription = table.Column<string> (nullable: true),
                        priority = table.Column<int> (nullable: false),
                        products = table.Column<string> (type: "jsonb", nullable : true),
                        firm_id = table.Column<int> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_article", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "author",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        title = table.Column<string> (maxLength: 256, nullable: false),
                        rewrite = table.Column<string> (maxLength: 256, nullable: false),
                        www = table.Column<string> (maxLength: 256, nullable: true),
                        email = table.Column<string> (maxLength: 256, nullable: true),
                        about = table.Column<string> (nullable: true),
                        images = table.Column<string> (nullable: true),
                        social = table.Column<string> (nullable: true),
                        sponsor = table.Column<string> (nullable: true),
                        display = table.Column<bool> (nullable: false),
                        firstname = table.Column<string> (maxLength: 256, nullable: true),
                        lastname = table.Column<string> (maxLength: 256, nullable: true),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying"),
                        description = table.Column<string> (nullable: true),
                        photo = table.Column<string> (type: "jsonb", nullable : true),
                        user_id = table.Column<int> (nullable: true),
                        password = table.Column<string> (maxLength: 32, nullable: true),
                        video_link = table.Column<string> (nullable: true),
                        logo = table.Column<string> (type: "jsonb", nullable : true),
                        background_logo = table.Column<string> (type: "jsonb", nullable : true),
                        about_blog_photo = table.Column<string> (type: "jsonb", nullable : true),
                        name = table.Column<string> (maxLength: 256, nullable: false, defaultValueSql: "''::character varying"),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        video_title = table.Column<string> (nullable: true),
                        hash = table.Column<string> (maxLength: 32, nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_author", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "category",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        parent_id = table.Column<int> (nullable: true),
                        orderby = table.Column<int> (nullable: false),
                        brief = table.Column<string> (nullable: true),
                        rewrite = table.Column<string> (maxLength: 256, nullable: true),
                        belongs_to_menu = table.Column<string> (nullable: true),
                        tags = table.Column<string> (type: "jsonb", nullable : true),
                        countries = table.Column<string> (type: "jsonb", nullable : true),
                        content = table.Column<string> (type: "jsonb", nullable : true),
                        with_content = table.Column<bool> (nullable: false),
                        images = table.Column<string> (type: "jsonb", nullable : true),
                        description = table.Column<string> (nullable: true),
                        _first_image_url = table.Column<string> (nullable: true),
                        color = table.Column<string> (nullable: true),
                        verify = table.Column<bool> (nullable: false),
                        firstimageurl = table.Column<string> (maxLength: 256, nullable: true),
                        path = table.Column<string> (maxLength: 256, nullable: true),
                        isvisible = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        title = table.Column<string> (maxLength: 256, nullable: true),
                        meta = table.Column<string> (type: "jsonb", nullable : true),
                        patron_id_article = table.Column<int> (nullable: true),
                        patron_id_shop = table.Column<int> (nullable: true),
                        priority = table.Column<int> (nullable: false, defaultValueSql: "1"),
                        is_fake = table.Column<bool> (nullable: false),
                        fake_tags = table.Column<string> (type: "jsonb", nullable : true),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : true, defaultValueSql: "'{}'::jsonb")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_category", x => x.id);
                    table.ForeignKey (
                        name: "category_parent_id_fkey",
                        column : x => x.parent_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "cms_menu",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        parent_id = table.Column<int> (nullable: true),
                        cms_page_identify = table.Column<int> (nullable: false),
                        name = table.Column<string> (maxLength: 256, nullable: true),
                        type = table.Column<string> (maxLength: 15, nullable: false, defaultValueSql: "'mainmenu'::character varying"),
                        orderby = table.Column<int> (nullable: false),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_cms_menu", x => x.id);
                    table.ForeignKey (
                        name: "cms_menu_parent_id_fkey",
                        column : x => x.parent_id,
                        principalTable: "cms_menu",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "cms_menu_type",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: true),
                        limit = table.Column<int> (nullable: false, defaultValueSql: "'-1'::integer"),
                        orderby = table.Column<int> (nullable: false),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_cms_menu_type", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "cms_page",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        region = table.Column<string> (maxLength: 128, nullable: true),
                        view = table.Column<string> (maxLength: 128, nullable: true),
                        title = table.Column<string> (maxLength: 128, nullable: false),
                        rewrite = table.Column<string> (maxLength: 128, nullable: false),
                        type = table.Column<string> (maxLength: 15, nullable: false, defaultValueSql: "'text'::character varying"),
                        metatitle = table.Column<string> (maxLength: 128, nullable: true),
                        metadescription = table.Column<string> (maxLength: 256, nullable: true),
                        metakeywords = table.Column<string> (maxLength: 256, nullable: true),
                        require_login = table.Column<bool> (nullable: false),
                        sections = table.Column<string> (type: "json", nullable : true, defaultValueSql: "'[]'::json"),
                        identify = table.Column<int> (nullable: false),
                        date_from = table.Column<DateTime> (nullable: false, defaultValueSql: "now()"),
                        content = table.Column<string> (nullable: true),
                        urlpatterns = table.Column<string> (type: "jsonb", nullable : true),
                        master_page_id = table.Column<int> (nullable: true),
                        hierarchy_parent_id = table.Column<int> (nullable: true),
                        breadcrumbs = table.Column<string> (maxLength: 128, nullable: true),
                        metafollow = table.Column<bool> (nullable: false, defaultValueSql: "true"),
                        metaindex = table.Column<bool> (nullable: false, defaultValueSql: "true"),
                        og = table.Column<string> (nullable: true),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_cms_page", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "cms_param",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        rewrite = table.Column<string> (maxLength: 2048, nullable: true),
                        value = table.Column<string> (type: "json", nullable : true),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_cms_param", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "country",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        code = table.Column<string> (maxLength: 2, nullable: false),
                        name = table.Column<string> (maxLength: 256, nullable: false, defaultValueSql: "'name'::character varying")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_country", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "firm",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        rewrite = table.Column<string> (maxLength: 256, nullable: false),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        about = table.Column<string> (nullable: true),
                        images = table.Column<string> (nullable: true),
                        logo = table.Column<string> (nullable: true),
                        contacts = table.Column<string> (type: "jsonb", nullable : true),
                        offer = table.Column<string> (nullable: true),
                        opened = table.Column<string> (nullable: true),
                        iban = table.Column<string> (maxLength: 26, nullable: true),
                        location = table.Column<string> (type: "jsonb", nullable : true),
                        background_logo = table.Column<string> (nullable: true),
                        nip = table.Column<string> (maxLength: 13, nullable: true),
                        is_accepted = table.Column<bool> (nullable: false),
                        about_firm_photo = table.Column<string> (nullable: true),
                        username = table.Column<string> (maxLength: 256, nullable: true),
                        password = table.Column<string> (maxLength: 32, nullable: true),
                        video_link = table.Column<string> (nullable: true),
                        valid_time = table.Column<DateTime> (type: "date", nullable : false, defaultValueSql: "(now())::timestamp without time zone"),
                        email = table.Column<string> (maxLength: 256, nullable: true),
                        hash = table.Column<string> (maxLength: 32, nullable: true),
                        blog = table.Column<string> (maxLength: 256, nullable: true),
                        type = table.Column<string> (maxLength: 256, nullable: true, defaultValueSql: "'Darmowe'::character varying"),
                        video_title = table.Column<string> (nullable: true),
                        checked_categories = table.Column<string> (type: "jsonb", nullable : true),
                        chacked_category = table.Column<string> (type: "jsonb", nullable : true),
                        custom_background_logo = table.Column<string> (type: "jsonb", nullable : true),
                        is_send_expire = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        date_tag_expire = table.Column<DateTime> (type: "date", nullable : true),
                        is_send_expire_1 = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        date_tag_expire_1 = table.Column<DateTime> (type: "date", nullable : true),
                        limit_products = table.Column<int> (nullable: true, defaultValueSql: "20"),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : true, defaultValueSql: "'{}'::jsonb"),
                        priority = table.Column<int> (nullable: false),
                        link_follow = table.Column<bool> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_firm", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "firm_gallery",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        group = table.Column<string> (maxLength: 256, nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_firm_gallery", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "import_stats",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        data = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_import_stats", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "label",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        unique = table.Column<string> (maxLength: 256, nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_label", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "landing_cookbooks",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        title = table.Column<string> (maxLength: 256, nullable: true),
                        cover = table.Column<string> (type: "jsonb", nullable : true),
                        publication_date = table.Column<DateTime> (type: "date", nullable : true),
                        file = table.Column<string> (type: "jsonb", nullable : true),
                        is_accepted = table.Column<bool> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_landing_cookbooks", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "mail",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        title = table.Column<string> (maxLength: 128, nullable: false),
                        content = table.Column<string> (nullable: false),
                        description = table.Column<string> (nullable: true),
                        type = table.Column<string> (maxLength: 256, nullable: false),
                        attachments = table.Column<string> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_mail", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "newsletter",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        email = table.Column<string> (maxLength: 256, nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_newsletter", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "patron",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        id_firm = table.Column<int> (nullable: true),
                        patron_name = table.Column<string> (nullable: true),
                        section_article = table.Column<bool> (nullable: true),
                        section_shop = table.Column<bool> (nullable: true),
                        category = table.Column<string> (type: "jsonb", nullable : true),
                        photo = table.Column<string> (type: "jsonb", nullable : true),
                        images = table.Column<string> (type: "jsonb", nullable : true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_patron", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "product_category",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 128, nullable: false),
                        rewrite = table.Column<string> (maxLength: 128, nullable: false),
                        parent_id = table.Column<int> (nullable: true),
                        orderby = table.Column<int> (nullable: true, defaultValueSql: "0"),
                        images = table.Column<string> (nullable: true),
                        description = table.Column<string> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_product_category", x => x.id);
                    table.ForeignKey (
                        name: "product_category_parent_id_fkey",
                        column : x => x.parent_id,
                        principalTable: "product_category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "redirect",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        product_id = table.Column<int> (nullable: false),
                        date_time = table.Column<DateTime> (type: "timestamp with time zone", nullable : false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_redirect", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "source",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        format = table.Column<string> (maxLength: 256, nullable: false),
                        imported_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        interval = table.Column<TimeSpan> (nullable: false, defaultValueSql: "'24:00:00'::interval"),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        active = table.Column<bool> (nullable: false),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_source", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "tag",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        unique = table.Column<string> (maxLength: 256, nullable: false),
                        parent_id = table.Column<int> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_tag", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "user",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        email = table.Column<string> (maxLength: 256, nullable: false),
                        active = table.Column<bool> (nullable: false),
                        password = table.Column<string> (maxLength: 32, nullable: true),
                        hash = table.Column<string> (maxLength: 32, nullable: true),
                        type = table.Column<string> (maxLength: 256, nullable: true, defaultValueSql: "'user'::character varying"),
                        name = table.Column<string> (maxLength: 256, nullable: true),
                        last_name = table.Column<string> (maxLength: 256, nullable: true),
                        nickname = table.Column<string> (maxLength: 256, nullable: true),
                        about = table.Column<string> (nullable: true),
                        images = table.Column<string> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_user", x => x.id);
                });

            migrationBuilder.CreateTable (
                name: "api_phrase",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        phrase = table.Column<string> (maxLength: 512, nullable: false),
                        search_in_name = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        exists_in_name = table.Column<bool> (nullable: true, defaultValueSql: "true"),
                        search_in_description = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        exists_in_description = table.Column<bool> (nullable: true, defaultValueSql: "true"),
                        category_id = table.Column<int> (nullable: true),
                        tags = table.Column<string> (type: "jsonb", nullable : true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_api_phrase", x => x.id);
                    table.ForeignKey (
                        name: "api_phrase_category_id_fkey",
                        column : x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "article_category",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 128, nullable: false),
                        rewrite = table.Column<string> (maxLength: 128, nullable: false),
                        parent_id = table.Column<int> (nullable: true),
                        orderby = table.Column<int> (nullable: true, defaultValueSql: "0"),
                        description = table.Column<string> (nullable: true),
                        isvisible = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        images = table.Column<string> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_article_category", x => x.id);
                    table.ForeignKey (
                        name: "article_category_parent_id_fkey",
                        column : x => x.parent_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "param",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        orderby = table.Column<int> (nullable: false),
                        is_sorted = table.Column<bool> (nullable: false),
                        is_filter = table.Column<bool> (nullable: false),
                        is_visible = table.Column<bool> (nullable: false),
                        unique = table.Column<string> (maxLength: 256, nullable: false),
                        category_id = table.Column<int> (nullable: false),
                        values = table.Column<string> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_param", x => x.id);
                    table.ForeignKey (
                        name: "param_category_id_fkey",
                        column : x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "product",
                columns : table => new {
                    id = table.Column<int> (nullable: false, defaultValueSql: "nextval('product_id_seq1'::regclass)"),
                        app = table.Column<string> (maxLength: 128, nullable: false, defaultValueSql: "'site'::character varying"),
                        rewrite = table.Column<string> (maxLength: 512, nullable: false),
                        category_id = table.Column<int> (nullable: false),
                        name = table.Column<string> (maxLength: 512, nullable: false),
                        description = table.Column<string> (nullable: false),
                        price = table.Column<decimal> (type: "numeric(10,2)", nullable : false),
                        oldprice = table.Column<decimal> (type: "numeric(10,2)", nullable : true, defaultValueSql: "0"),
                        quantity = table.Column<int> (nullable: false, defaultValueSql: "1"),
                        first_photo = table.Column<string> (nullable: true, defaultValueSql: "'[]'::text"),
                        photos_count = table.Column<int> (nullable: false),
                        url = table.Column<string> (maxLength: 2048, nullable: true),
                        is_available = table.Column<bool> (nullable: false, defaultValueSql: "true"),
                        coolness = table.Column<int> (nullable: false),
                        shows_count = table.Column<int> (nullable: false),
                        shop_id = table.Column<int> (nullable: true),
                        visible = table.Column<bool> (nullable: false, defaultValueSql: "true"),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : true, defaultValueSql: "'{\"manual\": {}, \"defined\": []}'::jsonb"),
                        photos = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'[]'::jsonb"),
                        tags = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'[]'::jsonb"),
                        labels = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'[]'::jsonb"),
                        accept_time = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        redirect_count = table.Column<int> (nullable: false),
                        fitured_date = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        featured = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        featured_date = table.Column<DateTime> (type: "timestamp with time zone", nullable : true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_product", x => x.id);
                    table.ForeignKey (
                        name: "product_category_id_fkey",
                        column : x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "product_param",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        type = table.Column<string> (type: "jsonb", nullable : false),
                        is_sorted = table.Column<bool> (nullable: false),
                        is_filter = table.Column<bool> (nullable: false),
                        is_visible = table.Column<bool> (nullable: false),
                        is_required = table.Column<bool> (nullable: false),
                        unique = table.Column<string> (maxLength: 256, nullable: false),
                        category_id = table.Column<int> (nullable: true),
                        values = table.Column<string> (type: "jsonb", nullable : true),
                        order_by = table.Column<int> (nullable: true, defaultValueSql: "0")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_product_param", x => x.id);
                    table.ForeignKey (
                        name: "product_param_category_id_fkey",
                        column : x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "usun_product",
                columns : table => new {
                    id = table.Column<int> (nullable: false, defaultValueSql: "nextval('product_id_seq'::regclass)"),
                        rewrite = table.Column<string> (maxLength: 256, nullable: false),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        content = table.Column<string> (maxLength: 256, nullable: false),
                        price = table.Column<double> (nullable: true),
                        packaging = table.Column<string> (nullable: true),
                        firm_id = table.Column<int> (nullable: true),
                        delivery_time = table.Column<int> (nullable: true),
                        notes = table.Column<string> (maxLength: 256, nullable: true),
                        images = table.Column<string> (nullable: true),
                        available = table.Column<bool> (nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_usun_product", x => x.id);
                    table.ForeignKey (
                        name: "product_firm_id_fkey",
                        column : x => x.firm_id,
                        principalTable: "firm",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "source_program",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        imported_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        interval = table.Column<TimeSpan> (nullable: false, defaultValueSql: "'24:00:00'::interval"),
                        raw = table.Column<string> (type: "jsonb", nullable : true, defaultValueSql: "'{}'::jsonb"),
                        source_id = table.Column<int> (nullable: false),
                        active = table.Column<bool> (nullable: false),
                        affiliation_link = table.Column<string> (maxLength: 128, nullable: true),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        shop_id = table.Column<int> (nullable: true),
                        last_import_time = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        info = table.Column<string> (nullable: true),
                        shop_disabled_date = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        shop_disabled = table.Column<bool> (nullable: true, defaultValueSql: "false")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_source_program", x => x.id);
                    table.ForeignKey (
                        name: "source_program_source_id_fkey",
                        column : x => x.source_id,
                        principalTable: "source",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "article_tag",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 256, nullable: false),
                        unique = table.Column<string> (maxLength: 256, nullable: false),
                        parent_id = table.Column<int> (nullable: true),
                        orderby = table.Column<int> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_article_tag", x => x.id);
                    table.ForeignKey (
                        name: "article_tag_parent_id_fkey",
                        column : x => x.parent_id,
                        principalTable: "tag",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "user_article_like",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        user_id = table.Column<int> (nullable: false),
                        article_id = table.Column<int> (nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_user_article_like", x => x.id);
                    table.ForeignKey (
                        name: "user_article_like_article_id_fkey",
                        column : x => x.article_id,
                        principalTable: "article",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey (
                        name: "user_article_like_user_id_fkey",
                        column : x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "source_category",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        name = table.Column<string> (maxLength: 250, nullable: false),
                        source_program_id = table.Column<int> (nullable: false),
                        target_category_id = table.Column<int> (nullable: true),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        import = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        hidden = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : true, defaultValueSql: "'{}'::jsonb"),
                        auto_transfer = table.Column<bool> (nullable: true, defaultValueSql: "false")
                },
                constraints : table => {
                    table.PrimaryKey ("PK_source_category", x => x.id);
                    table.ForeignKey (
                        name: "source_category_source_program_id_fkey",
                        column : x => x.source_program_id,
                        principalTable: "source_program",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey (
                        name: "source_category_target_category_id_fkey",
                        column : x => x.target_category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable (
                name: "source_offer",
                columns : table => new {
                    id = table.Column<int> (nullable: false)
                        .Annotation ("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                        content = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        @params = table.Column<string> (name: "params", type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        imported_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        raw = table.Column<string> (type: "jsonb", nullable : false, defaultValueSql: "'{}'::jsonb"),
                        source_program_id = table.Column<int> (nullable: false),
                        product_id = table.Column<int> (nullable: true),
                        category_id = table.Column<int> (nullable: true),
                        active = table.Column<bool> (nullable: true, defaultValueSql: "false"),
                        state = table.Column<bool> (nullable: true),
                        changed = table.Column<DateTime> (type: "timestamp with time zone", nullable : true),
                        source_category_id = table.Column<int> (nullable: true),
                        availability = table.Column<bool> (nullable: false, defaultValueSql: "true"),
                        created = table.Column<DateTime> (type: "timestamp with time zone", nullable : true, defaultValueSql: "'2017-11-27 10:16:51.946073+01'::timestamp with time zone"),
                        create_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        update_at = table.Column<DateTime> (type: "timestamp with time zone", nullable : false, defaultValueSql: "now()"),
                        manufacturer = table.Column<string> (maxLength: 1042, nullable: true),
                        xid = table.Column<string> (maxLength: 1042, nullable: true),
                        content_hash = table.Column<string> (maxLength: 1042, nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey ("PK_source_offer", x => x.id);
                    table.ForeignKey (
                        name: "source_offer_category_id_fkey",
                        column : x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey (
                        name: "source_offer_product_id_fkey",
                        column : x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey (
                        name: "source_offer_source_program_id_fkey",
                        column : x => x.source_program_id,
                        principalTable: "source_program",
                        principalColumn: "id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex (
                name: "admin_username_key",
                table: "admin",
                column: "username",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "advertise_name_key",
                table: "advertise",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "api_code_key",
                table: "api",
                column: "code",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "api_name_key",
                table: "api",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "api_url_key",
                table: "api",
                column: "url",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_api_phrase_category_id",
                table: "api_phrase",
                column: "category_id");

            migrationBuilder.CreateIndex (
                name: "article_title_key",
                table: "article",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "article_rewrite_key",
                table: "article",
                column: "rewrite",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_article_category_parent_id",
                table: "article_category",
                column: "parent_id");

            migrationBuilder.CreateIndex (
                name: "article_tag_name_key",
                table: "article_tag",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_article_tag_parent_id",
                table: "article_tag",
                column: "parent_id");

            migrationBuilder.CreateIndex (
                name: "article_tag_unique_key",
                table: "article_tag",
                column: "unique",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "author_rewrite_key",
                table: "author",
                column: "rewrite",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "author_title_key",
                table: "author",
                column: "title",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "category_name_key",
                table: "category",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_category_parent_id",
                table: "category",
                column: "parent_id");

            migrationBuilder.CreateIndex (
                name: "IX_cms_menu_parent_id",
                table: "cms_menu",
                column: "parent_id");

            migrationBuilder.CreateIndex (
                name: "cms_param_rewrite_uq",
                table: "cms_param",
                column: "rewrite",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "country_name_key",
                table: "country",
                column: "code",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "index_name",
                table: "country",
                column: "name");

            migrationBuilder.CreateIndex (
                name: "firm_rewrite_key",
                table: "firm",
                column: "rewrite",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "label_name_key",
                table: "label",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "label_unique_key",
                table: "label",
                column: "unique",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "mail_type_key",
                table: "mail",
                column: "type",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_param_category_id",
                table: "param",
                column: "category_id");

            migrationBuilder.CreateIndex (
                name: "IX_product_category_id",
                table: "product",
                column: "category_id");

            migrationBuilder.CreateIndex (
                name: "product_rewrite_key1",
                table: "product",
                column: "rewrite",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "IX_product_category_parent_id",
                table: "product_category",
                column: "parent_id");

            migrationBuilder.CreateIndex (
                name: "IX_product_param_category_id",
                table: "product_param",
                column: "category_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_category_source_program_id",
                table: "source_category",
                column: "source_program_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_category_target_category_id",
                table: "source_category",
                column: "target_category_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_offer_category_id",
                table: "source_offer",
                column: "category_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_offer_product_id",
                table: "source_offer",
                column: "product_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_offer_source_program_id",
                table: "source_offer",
                column: "source_program_id");

            migrationBuilder.CreateIndex (
                name: "IX_source_program_source_id",
                table: "source_program",
                column: "source_id");

            migrationBuilder.CreateIndex (
                name: "tag_name_key",
                table: "tag",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "index_unique",
                table: "tag",
                column: "unique");

            migrationBuilder.CreateIndex (
                name: "IX_user_article_like_article_id",
                table: "user_article_like",
                column: "article_id");

            migrationBuilder.CreateIndex (
                name: "IX_user_article_like_user_id",
                table: "user_article_like",
                column: "user_id");

            migrationBuilder.CreateIndex (
                name: "IX_usun_product_firm_id",
                table: "usun_product",
                column: "firm_id");

            migrationBuilder.CreateIndex (
                name: "product_name_key",
                table: "usun_product",
                column: "name",
                unique : true);

            migrationBuilder.CreateIndex (
                name: "product_rewrite_key",
                table: "usun_product",
                column: "rewrite",
                unique : true);
        }

        protected override void Down (MigrationBuilder migrationBuilder) {
            migrationBuilder.DropTable (
                name: "admin");

            migrationBuilder.DropTable (
                name: "advertise");

            migrationBuilder.DropTable (
                name: "api");

            migrationBuilder.DropTable (
                name: "api_phrase");

            migrationBuilder.DropTable (
                name: "article_category");

            migrationBuilder.DropTable (
                name: "article_tag");

            migrationBuilder.DropTable (
                name: "author");

            migrationBuilder.DropTable (
                name: "cms_menu");

            migrationBuilder.DropTable (
                name: "cms_menu_type");

            migrationBuilder.DropTable (
                name: "cms_page");

            migrationBuilder.DropTable (
                name: "cms_param");

            migrationBuilder.DropTable (
                name: "country");

            migrationBuilder.DropTable (
                name: "firm_gallery");

            migrationBuilder.DropTable (
                name: "import_stats");

            migrationBuilder.DropTable (
                name: "label");

            migrationBuilder.DropTable (
                name: "landing_cookbooks");

            migrationBuilder.DropTable (
                name: "mail");

            migrationBuilder.DropTable (
                name: "newsletter");

            migrationBuilder.DropTable (
                name: "param");

            migrationBuilder.DropTable (
                name: "patron");

            migrationBuilder.DropTable (
                name: "product_category");

            migrationBuilder.DropTable (
                name: "product_param");

            migrationBuilder.DropTable (
                name: "redirect");

            migrationBuilder.DropTable (
                name: "source_category");

            migrationBuilder.DropTable (
                name: "source_offer");

            migrationBuilder.DropTable (
                name: "user_article_like");

            migrationBuilder.DropTable (
                name: "usun_product");

            migrationBuilder.DropTable (
                name: "tag");

            migrationBuilder.DropTable (
                name: "product");

            migrationBuilder.DropTable (
                name: "source_program");

            migrationBuilder.DropTable (
                name: "article");

            migrationBuilder.DropTable (
                name: "user");

            migrationBuilder.DropTable (
                name: "firm");

            migrationBuilder.DropTable (
                name: "category");

            migrationBuilder.DropTable (
                name: "source");

            migrationBuilder.DropSequence (
                name: "product_id_seq");

            migrationBuilder.DropSequence (
                name: "product_id_seq1");
        }
    }
}