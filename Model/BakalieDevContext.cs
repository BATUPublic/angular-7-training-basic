﻿using System;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BATUSystems.AdminPanel.Model {
    public partial class BakalieDevContext : DbContext {
        public BakalieDevContext () { }

        public BakalieDevContext (DbContextOptions<BakalieDevContext> options) : base (options) { }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<Advertise> Advertise { get; set; }
        public virtual DbSet<Api> Api { get; set; }
        public virtual DbSet<ApiPhrase> ApiPhrase { get; set; }
        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategory { get; set; }
        public virtual DbSet<ArticleTag> ArticleTag { get; set; }
        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<CmsMenu> CmsMenu { get; set; }
        public virtual DbSet<CmsMenuType> CmsMenuType { get; set; }
        public virtual DbSet<CmsPage> CmsPage { get; set; }
        public virtual DbSet<CmsParam> CmsParam { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Firm> Firm { get; set; }
        public virtual DbSet<FirmGallery> FirmGallery { get; set; }
        public virtual DbSet<ImportStats> ImportStats { get; set; }
        public virtual DbSet<Label> Label { get; set; }
        public virtual DbSet<LandingCookbooks> LandingCookbooks { get; set; }
        public virtual DbSet<Mail> Mail { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Newsletter> Newsletter { get; set; }
        public virtual DbSet<Param> Param { get; set; }
        public virtual DbSet<Patron> Patron { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }
        public virtual DbSet<ProductParam> ProductParam { get; set; }
        public virtual DbSet<Redirect> Redirect { get; set; }
        public virtual DbSet<Source> Source { get; set; }
        public virtual DbSet<SourceCategory> SourceCategory { get; set; }
        public virtual DbSet<SourceOffer> SourceOffer { get; set; }
        public virtual DbSet<SourceProgram> SourceProgram { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserArticleLike> UserArticleLike { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            if (!optionsBuilder.IsConfigured) {
                optionsBuilder.UseNpgsql ("Host=vps1.batusystems.pl;Database=bakalie_dev;Username=developer;Password=kurwa123");
            }
        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.HasPostgresExtension ("plv8");

            modelBuilder.Entity<Admin> (entity => {
                entity.HasIndex (e => e.Username)
                    .HasName ("admin_username_key")
                    .IsUnique ();

                entity.Property (e => e._groups).HasDefaultValueSql ("'[]'::jsonb");
            });

            modelBuilder.Entity<Advertise> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("advertise_name_key")
                    .IsUnique ();
            });

            modelBuilder.Entity<Api> (entity => {
                entity.HasIndex (e => e.Code)
                    .HasName ("api_code_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Name)
                    .HasName ("api_name_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Url)
                    .HasName ("api_url_key")
                    .IsUnique ();
            });

            modelBuilder.Entity<ApiPhrase> (entity => {
                entity.Property (e => e.ExistsInDescription)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.ExistsInName)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.SearchInDescription)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.SearchInName)
                    .HasDefaultValueSql ("false");

                entity.HasOne (d => d.Category)
                    .WithMany (p => p.ApiPhrase)
                    .HasForeignKey (d => d.CategoryId)
                    .HasConstraintName ("api_phrase_category_id_fkey");
            });

            modelBuilder.Entity<Article> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("article_title_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Rewrite)
                    .HasName ("article_rewrite_key")
                    .IsUnique ();

                entity.Property (e => e.CategoryId)
                    .HasDefaultValueSql ("1");

                entity.Property (e => e.PublicationDate)
                    .HasDefaultValueSql ("(now())::timestamp without time zone");

                entity.Property (e => e.ShowsCount)
                    .HasDefaultValueSql ("0");

                entity.Property (e => e.Type)
                    .HasDefaultValueSql ("'info'::character varying");
            });

            modelBuilder.Entity<ArticleCategory> (entity => {

                entity.Property (e => e.Isvisible)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.Orderby)
                    .HasDefaultValueSql ("0");

                entity.HasOne (d => d.Parent)
                    .WithMany (p => p.ArticleCategory)
                    .HasForeignKey (d => d.ParentId)
                    .HasConstraintName ("article_category_parent_id_fkey");
            });

            modelBuilder.Entity<ArticleTag> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("article_tag_name_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Unique)
                    .HasName ("article_tag_unique_key")
                    .IsUnique ();

                entity.HasOne (d => d.Parent)
                    .WithMany (p => p.ArticleTag)
                    .HasForeignKey (d => d.ParentId)
                    .HasConstraintName ("article_tag_parent_id_fkey");
            });

            modelBuilder.Entity<Author> (entity => {
                entity.HasIndex (e => e.Rewrite)
                    .HasName ("author_rewrite_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Title)
                    .HasName ("author_title_key")
                    .IsUnique ();

                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Name)
                    .HasDefaultValueSql ("''::character varying");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");

            });

            modelBuilder.Entity<Category> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("category_name_key")
                    .IsUnique ();

                entity.Property (e => e.Firstimageurl1);

                entity.Property (e => e.Isvisible)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.Priority).HasDefaultValueSql ("1");

                entity.HasOne (d => d.Parent)
                    .WithMany (p => p.InverseParent)
                    .HasForeignKey (d => d.ParentId)
                    .HasConstraintName ("category_parent_id_fkey");
            });

            modelBuilder.Entity<CmsMenu> (entity => {
                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");

                entity.Property (e => e.Type).HasDefaultValueSql ("'mainmenu'::character varying");

                entity.HasOne (d => d.Parent)
                    .WithMany (p => p.InverseParent)
                    .HasForeignKey (d => d.ParentId)
                    .HasConstraintName ("cms_menu_parent_id_fkey");
            });

            modelBuilder.Entity<CmsMenuType> (entity => {

                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");

                entity.Property (e => e.Limit)
                    .HasDefaultValueSql ("'-1'::integer");

            });

            modelBuilder.Entity<CmsPage> (entity => {
                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");

                entity.Property (e => e.DateFrom).HasDefaultValueSql ("now()");

                entity.Property (e => e.Metafollow)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.Metaindex)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.Sections).HasDefaultValueSql ("'[]'::json");

                entity.Property (e => e.Type).HasDefaultValueSql ("'text'::character varying");

            });

            modelBuilder.Entity<CmsParam> (entity => {
                entity.HasIndex (e => e.Rewrite)
                    .HasName ("cms_param_rewrite_uq")
                    .IsUnique ();

                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");
            });

            modelBuilder.Entity<Country> (entity => {
                entity.HasIndex (e => e.Code)
                    .HasName ("country_name_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Name)
                    .HasName ("index_name");

                entity.Property (e => e.Name).HasDefaultValueSql ("'name'::character varying");
            });

            modelBuilder.Entity<Firm> (entity => {
                entity.HasIndex (e => e.Rewrite)
                    .HasName ("firm_rewrite_key")
                    .IsUnique ();

                entity.Property (e => e.DateTagExpire1);

                entity.Property (e => e.IsSendExpire)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.IsSendExpire1)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.LimitProducts)
                    .HasDefaultValueSql ("20");

                entity.Property (e => e.Params).HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.Type)
                    .HasDefaultValueSql ("'Darmowe'::character varying");

                entity.Property (e => e.ValidTime)
                    .HasDefaultValueSql ("(now())::timestamp without time zone");

            });

            modelBuilder.Entity<FirmGallery> (entity => {

            });

            modelBuilder.Entity<ImportStats> (entity => {

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Data)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");
            });

            modelBuilder.Entity<Label> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("label_name_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Unique)
                    .HasName ("label_unique_key")
                    .IsUnique ();

            });

            modelBuilder.Entity<LandingCookbooks> (entity => {

            });

            modelBuilder.Entity<Mail> (entity => {
                entity.HasIndex (e => e.Type)
                    .HasName ("mail_type_key")
                    .IsUnique ();

            });

            modelBuilder.Entity<Message> (entity => {
                entity.Property (b => b.Stared)
                    .HasDefaultValueSql ("false");

                entity.Property (b => b.Important)
                    .HasDefaultValueSql ("false");
            });

            modelBuilder.Entity<Newsletter> (entity => {

            });

            modelBuilder.Entity<Param> (entity => {

                entity.HasOne (d => d.Category)
                    .WithMany (p => p.Param)
                    .HasForeignKey (d => d.CategoryId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("param_category_id_fkey");
            });

            modelBuilder.Entity<Patron> (entity => {

            });

            modelBuilder.Entity<Product> (entity => {
                entity.HasIndex (e => e.Rewrite)
                    .HasName ("product_rewrite_key1")
                    .IsUnique ();

                entity.Property (e => e.Id)
                    .HasDefaultValueSql ("nextval('product_id_seq1'::regclass)");

                entity.Property (e => e.AcceptTime)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.App)
                    .HasDefaultValueSql ("'site'::character varying");

                entity.Property (e => e.FirstPhoto)
                    .HasDefaultValueSql ("'[]'::text");

                entity.Property (e => e.IsAvailable)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.Labels)
                    .HasDefaultValueSql ("'[]'::jsonb");

                entity.Property (e => e.Oldprice)
                    .HasDefaultValueSql ("0");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{\"manual\": {}, \"defined\": []}'::jsonb");

                entity.Property (e => e.Photos).HasDefaultValueSql ("'[]'::jsonb");

                entity.Property (e => e.Quantity)
                    .HasDefaultValueSql ("1");

                entity.Property (e => e.Tags).HasDefaultValueSql ("'[]'::jsonb");

                entity.Property (e => e.Visible)
                    .HasDefaultValueSql ("true");

                entity.HasOne (d => d.Category)
                    .WithMany (p => p.Product)
                    .HasForeignKey (d => d.CategoryId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("product_category_id_fkey");
            });

            modelBuilder.Entity<ProductCategory> (entity => {

                entity.Property (e => e.Orderby)
                    .HasDefaultValueSql ("0");

                entity.HasOne (d => d.Parent)
                    .WithMany (p => p.InverseParent)
                    .HasForeignKey (d => d.ParentId)
                    .HasConstraintName ("product_category_parent_id_fkey");
            });

            modelBuilder.Entity<ProductParam> (entity => {

                entity.Property (e => e.OrderBy)
                    .HasDefaultValueSql ("0");

                entity.HasOne (d => d.Category)
                    .WithMany (p => p.ProductParam)
                    .HasForeignKey (d => d.CategoryId)
                    .HasConstraintName ("product_param_category_id_fkey");
            });

            modelBuilder.Entity<Redirect> (entity => {

            });

            modelBuilder.Entity<Source> (entity => {

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Interval)
                    .HasDefaultValueSql ("'24:00:00'::interval");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");
            });

            modelBuilder.Entity<SourceCategory> (entity => {

                entity.Property (e => e.AutoTransfer)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Hidden)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.Import)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");

                entity.HasOne (d => d.SourceProgram)
                    .WithMany (p => p.SourceCategory)
                    .HasForeignKey (d => d.SourceProgramId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("source_category_source_program_id_fkey");

                entity.HasOne (d => d.TargetCategory)
                    .WithMany (p => p.SourceCategory)
                    .HasForeignKey (d => d.TargetCategoryId)
                    .HasConstraintName ("source_category_target_category_id_fkey");
            });

            modelBuilder.Entity<SourceOffer> (entity => {

                entity.Property (e => e.Active)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.Availability)
                    .HasDefaultValueSql ("true");

                entity.Property (e => e.Content)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Created)
                    .HasDefaultValueSql ("'2017-11-27 10:16:51.946073+01'::timestamp with time zone");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.Raw)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");

                entity.HasOne (d => d.Category)
                    .WithMany (p => p.SourceOffer)
                    .HasForeignKey (d => d.CategoryId)
                    .HasConstraintName ("source_offer_category_id_fkey");

                entity.HasOne (d => d.Product)
                    .WithMany (p => p.SourceOffer)
                    .HasForeignKey (d => d.ProductId)
                    .HasConstraintName ("source_offer_product_id_fkey");

                entity.HasOne (d => d.SourceProgram)
                    .WithMany (p => p.SourceOffer)
                    .HasForeignKey (d => d.SourceProgramId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("source_offer_source_program_id_fkey");
            });

            modelBuilder.Entity<SourceProgram> (entity => {

                entity.Property (e => e.CreateAt)
                    .HasDefaultValueSql ("now()");

                entity.Property (e => e.Interval)
                    .HasDefaultValueSql ("'24:00:00'::interval");

                entity.Property (e => e.Params)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.Raw)
                    .HasDefaultValueSql ("'{}'::jsonb");

                entity.Property (e => e.ShopDisabled)
                    .HasDefaultValueSql ("false");

                entity.Property (e => e.UpdateAt)
                    .HasDefaultValueSql ("now()");

                entity.HasOne (d => d.Source)
                    .WithMany (p => p.SourceProgram)
                    .HasForeignKey (d => d.SourceId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("source_program_source_id_fkey");
            });

            modelBuilder.Entity<Tag> (entity => {
                entity.HasIndex (e => e.Name)
                    .HasName ("tag_name_key")
                    .IsUnique ();

                entity.HasIndex (e => e.Unique)
                    .HasName ("index_unique");

            });

            modelBuilder.Entity<User> (entity => {

                entity.Property (e => e.Type)
                    .HasDefaultValueSql ("'user'::character varying");
            });

            modelBuilder.Entity<UserArticleLike> (entity => {

                entity.HasOne (d => d.Article)
                    .WithMany (p => p.UserArticleLike)
                    .HasForeignKey (d => d.ArticleId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("user_article_like_article_id_fkey");

                entity.HasOne (d => d.User)
                    .WithMany (p => p.UserArticleLike)
                    .HasForeignKey (d => d.UserId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("user_article_like_user_id_fkey");
            });

            modelBuilder.HasSequence<int> ("product_id_seq");
        }
    }
}