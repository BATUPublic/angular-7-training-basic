﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("tag")]
    public partial class Tag {
        public Tag () {
            ArticleTag = new HashSet<ArticleTag> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("unique")]
        [StringLength (256)]
        public string Unique { get; set; }

        [Column ("parent_id")]
        public int? ParentId { get; set; }

        [InverseProperty ("Parent")]
        public ICollection<ArticleTag> ArticleTag { get; set; }
    }
}