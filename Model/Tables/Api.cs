﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("api")]
    public partial class Api {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (128)]
        public string Name { get; set; }

        [Required]
        [Column ("code")]
        [StringLength (128)]
        public string Code { get; set; }

        [Required]
        [Column ("url")]
        [StringLength (256)]
        public string Url { get; set; }

        [Column ("orderby")]
        public int Orderby { get; set; }
    }
}