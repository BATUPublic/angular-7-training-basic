﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("import_stats")]
    public partial class ImportStats {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("data", TypeName = "jsonb")]
        public string Data { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }
    }
}