﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("firm_gallery")]
    public partial class FirmGallery {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("group")]
        [StringLength (256)]
        public string Group { get; set; }
    }
}