﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("user")]
    public partial class User {
        public User () {
            UserArticleLike = new HashSet<UserArticleLike> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("email")]
        [StringLength (256)]
        public string Email { get; set; }

        [Column ("active")]
        public bool Active { get; set; }

        [Column ("password")]
        [StringLength (32)]
        public string Password { get; set; }

        [Column ("hash")]
        [StringLength (32)]
        public string Hash { get; set; }

        [Column ("type")]
        [StringLength (256)]
        public string Type { get; set; }

        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("last_name")]
        [StringLength (256)]
        public string LastName { get; set; }

        [Column ("nickname")]
        [StringLength (256)]
        public string Nickname { get; set; }

        [Column ("about")]
        public string About { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [InverseProperty ("User")]
        public ICollection<UserArticleLike> UserArticleLike { get; set; }
    }
}