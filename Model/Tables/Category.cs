﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("category")]
    public partial class Category {
        public Category () {
            ApiPhrase = new HashSet<ApiPhrase> ();
            ArticleCategory = new HashSet<ArticleCategory> ();
            InverseParent = new HashSet<Category> ();
            Param = new HashSet<Param> ();
            Product = new HashSet<Product> ();
            ProductParam = new HashSet<ProductParam> ();
            SourceCategory = new HashSet<SourceCategory> ();
            SourceOffer = new HashSet<SourceOffer> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("parent_id")]
        public int? ParentId { get; set; }

        [Column ("orderby")]
        public int Orderby { get; set; }

        [Column ("brief")]
        public string Brief { get; set; }

        [Column ("rewrite")]
        [StringLength (256)]
        public string Rewrite { get; set; }

        [Column ("belongs_to_menu")]
        public string BelongsToMenu { get; set; }

        [Column ("tags", TypeName = "jsonb")]
        public string Tags { get; set; }

        [Column ("countries", TypeName = "jsonb")]
        public string Countries { get; set; }

        [Column ("content", TypeName = "jsonb")]
        public string Content { get; set; }

        [Column ("with_content")]
        public bool WithContent { get; set; }

        [Column ("images", TypeName = "jsonb")]
        public string Images { get; set; }

        [Column ("description")]
        public string Description { get; set; }

        [Column ("_first_image_url")]
        public string FirstImageUrl { get; set; }

        [Column ("color")]
        public string Color { get; set; }

        [Column ("verify")]
        public bool Verify { get; set; }

        [Column ("firstimageurl")]
        [StringLength (256)]
        public string Firstimageurl1 { get; set; }

        [Column ("path")]
        [StringLength (256)]
        public string Path { get; set; }

        [Column ("isvisible")]
        public bool? Isvisible { get; set; }

        [Column ("title")]
        [StringLength (256)]
        public string Title { get; set; }

        [Column ("meta", TypeName = "jsonb")]
        public string Meta { get; set; }

        [Column ("patron_id_article")]
        public int? PatronIdArticle { get; set; }

        [Column ("patron_id_shop")]
        public int? PatronIdShop { get; set; }

        [Column ("priority")]
        public int Priority { get; set; }

        [Column ("is_fake")]
        public bool IsFake { get; set; }

        [Column ("fake_tags", TypeName = "jsonb")]
        public string FakeTags { get; set; }

        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [ForeignKey ("ParentId")]
        [InverseProperty ("InverseParent")]
        public Category Parent { get; set; }

        [InverseProperty ("Category")]
        public ICollection<ApiPhrase> ApiPhrase { get; set; }

        [InverseProperty ("Parent")]
        public ICollection<ArticleCategory> ArticleCategory { get; set; }

        [InverseProperty ("Parent")]
        public ICollection<Category> InverseParent { get; set; }

        [InverseProperty ("Category")]
        public ICollection<Param> Param { get; set; }

        [InverseProperty ("Category")]
        public ICollection<Product> Product { get; set; }

        [InverseProperty ("Category")]
        public ICollection<ProductParam> ProductParam { get; set; }

        [InverseProperty ("TargetCategory")]
        public ICollection<SourceCategory> SourceCategory { get; set; }

        [InverseProperty ("Category")]
        public ICollection<SourceOffer> SourceOffer { get; set; }
    }
}