﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("firm")]
    public partial class Firm {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (256)]
        public string Rewrite { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("about")]
        public string About { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [Column ("logo")]
        public string Logo { get; set; }

        [Column ("contacts", TypeName = "jsonb")]
        public string Contacts { get; set; }

        [Column ("offer")]
        public string Offer { get; set; }

        [Column ("opened")]
        public string Opened { get; set; }

        [Column ("iban")]
        [StringLength (26)]
        public string Iban { get; set; }

        [Column ("location", TypeName = "jsonb")]
        public string Location { get; set; }

        [Column ("background_logo")]
        public string BackgroundLogo { get; set; }

        [Column ("nip")]
        [StringLength (13)]
        public string Nip { get; set; }

        [Column ("is_accepted")]
        public bool IsAccepted { get; set; }

        [Column ("about_firm_photo")]
        public string AboutFirmPhoto { get; set; }

        [Column ("username")]
        [StringLength (256)]
        public string Username { get; set; }

        [Column ("password")]
        [StringLength (32)]
        public string Password { get; set; }

        [Column ("video_link")]
        public string VideoLink { get; set; }

        [Column ("valid_time", TypeName = "date")]
        public DateTime ValidTime { get; set; }

        [Column ("email")]
        [StringLength (256)]
        public string Email { get; set; }

        [Column ("hash")]
        [StringLength (32)]
        public string Hash { get; set; }

        [Column ("blog")]
        [StringLength (256)]
        public string Blog { get; set; }

        [Column ("type")]
        [StringLength (256)]
        public string Type { get; set; }

        [Column ("video_title")]
        public string VideoTitle { get; set; }

        [Column ("checked_categories", TypeName = "jsonb")]
        public string CheckedCategories { get; set; }

        [Column ("chacked_category", TypeName = "jsonb")]
        public string ChackedCategory { get; set; }

        [Column ("custom_background_logo", TypeName = "jsonb")]
        public string CustomBackgroundLogo { get; set; }

        [Column ("is_send_expire")]
        public bool? IsSendExpire { get; set; }

        [Column ("date_tag_expire", TypeName = "date")]
        public DateTime? DateTagExpire { get; set; }

        [Column ("is_send_expire_1")]
        public bool? IsSendExpire1 { get; set; }

        [Column ("date_tag_expire_1", TypeName = "date")]
        public DateTime? DateTagExpire1 { get; set; }

        [Column ("limit_products")]
        public int? LimitProducts { get; set; }

        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Column ("priority")]
        public int Priority { get; set; }

        [Column ("link_follow")]
        public bool LinkFollow { get; set; }
    }
}