﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("message")]
    public partial class Message {
        [Key]
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("from_id")]
        [ForeignKey ("From")]
        public int FromId { get; set; }
        public Admin From { get; set; }
        // public Admin From {
        //     get => LazyLoader.Load (this, ref _from);
        //     set => _from = value;
        // }
        // private Admin _from;

        [Required]
        [Column ("to_id")]
        [ForeignKey ("To")]
        public int ToId { get; set; }
        public Admin To { get; set; }
        // public Admin To {
        //     get => LazyLoader.Load (this, ref _to);
        //     set => _to = value;
        // }
        // private Admin _to;

        [Column ("topic")]
        [StringLength (256)]
        public string Topic { get; set; }

        [Column ("content")]
        public string Content { get; set; }

        [Column ("sent", TypeName = "timestamp with time zone")]
        public DateTime Sent { get; set; }

        [Column ("read", TypeName = "timestamp with time zone")]
        public DateTime? Read { get; set; }

        [Column ("stared")]
        public bool? Stared { get; set; }

        [Column ("important")]
        public bool? Important { get; set; }

        [Column ("deleted")]
        public DateTime? Deleted { get; set; }

        [Column ("labels", TypeName = "jsonb")]
        [JsonIgnore]
        public string _labels { get; set; }

        [NotMapped]
        public JArray Labels {
            get {
                return JsonConvert.DeserializeObject<JArray> (string.IsNullOrEmpty (_labels) ? "[]" : _labels);
            }
            set {
                _labels = JsonConvert.SerializeObject (value);
            }
        }
    }
}