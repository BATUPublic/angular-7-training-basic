﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("source")]
    public partial class Source {
        public Source () {
            SourceProgram = new HashSet<SourceProgram> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("format")]
        [StringLength (256)]
        public string Format { get; set; }

        [Column ("imported_at", TypeName = "timestamp with time zone")]
        public DateTime? ImportedAt { get; set; }

        [Column ("interval")]
        public TimeSpan Interval { get; set; }

        [Required]
        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Column ("active")]
        public bool Active { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }

        [InverseProperty ("Source")]
        public ICollection<SourceProgram> SourceProgram { get; set; }
    }
}