﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("article")]
    public partial class Article {
        public Article () {
            UserArticleLike = new HashSet<UserArticleLike> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("content")]
        public string Content { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (256)]
        public string Rewrite { get; set; }

        [Column ("category_id")]
        public int CategoryId { get; set; }

        [Column ("instrumental")]
        [StringLength (256)]
        public string Instrumental { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [Column ("tags")]
        public string Tags { get; set; }

        [Required]
        [Column ("type")]
        [StringLength (64)]
        public string Type { get; set; }

        [Column ("countries")]
        public string Countries { get; set; }

        [Column ("slider1")]
        public string Slider1 { get; set; }

        [Column ("slider2")]
        public string Slider2 { get; set; }

        [Column ("categories", TypeName = "json")]
        public string Categories { get; set; }

        [Column ("author_id")]
        public int? AuthorId { get; set; }

        [Column ("verify")]
        public bool Verify { get; set; }

        [Column ("verification_date", TypeName = "date")]
        public DateTime? VerificationDate { get; set; }

        [Column ("_images_count")]
        public int? ImagesCount { get; set; }

        [Column ("_tags_count")]
        public int? TagsCount { get; set; }

        [Column ("_countries_count")]
        public int? CountriesCount { get; set; }

        [Column ("publication_date", TypeName = "date")]
        public DateTime PublicationDate { get; set; }

        [Column ("img_transfer")]
        public bool? ImgTransfer { get; set; }

        [Column ("_first_image_url")]
        public string FirstImageUrl { get; set; }

        [Column ("shows_count")]
        public int? ShowsCount { get; set; }

        [Column ("brief")]
        public string Brief { get; set; }

        [Column ("firstimageurl")]
        [StringLength (256)]
        public string Firstimageurl1 { get; set; }

        [Column ("metadescription")]
        public string Metadescription { get; set; }

        [Column ("priority")]
        public int Priority { get; set; }

        [Column ("products", TypeName = "jsonb")]
        public string Products { get; set; }

        [Column ("firm_id")]
        public int? FirmId { get; set; }

        [InverseProperty ("Article")]
        public ICollection<UserArticleLike> UserArticleLike { get; set; }
    }
}