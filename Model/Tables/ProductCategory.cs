﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("product_category")]
    public partial class ProductCategory {
        public ProductCategory () {
            InverseParent = new HashSet<ProductCategory> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (128)]
        public string Name { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (128)]
        public string Rewrite { get; set; }

        [Column ("parent_id")]
        public int? ParentId { get; set; }

        [Column ("orderby")]
        public int? Orderby { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [Column ("description")]
        public string Description { get; set; }

        [ForeignKey ("ParentId")]
        [InverseProperty ("InverseParent")]
        public ProductCategory Parent { get; set; }

        [InverseProperty ("Parent")]
        public ICollection<ProductCategory> InverseParent { get; set; }
    }
}