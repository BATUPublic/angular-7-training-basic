﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("source_offer")]
    public partial class SourceOffer {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("content", TypeName = "jsonb")]
        public string Content { get; set; }

        [Required]
        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Column ("imported_at", TypeName = "timestamp with time zone")]
        public DateTime? ImportedAt { get; set; }

        [Required]
        [Column ("raw", TypeName = "jsonb")]
        public string Raw { get; set; }

        [Column ("source_program_id")]
        public int SourceProgramId { get; set; }

        [Column ("product_id")]
        public int? ProductId { get; set; }

        [Column ("category_id")]
        public int? CategoryId { get; set; }

        [Column ("active")]
        public bool? Active { get; set; }

        [Column ("state")]
        public bool? State { get; set; }

        [Column ("changed", TypeName = "timestamp with time zone")]
        public DateTime? Changed { get; set; }

        [Column ("source_category_id")]
        public int? SourceCategoryId { get; set; }

        [Required]
        [Column ("availability")]
        public bool? Availability { get; set; }

        [Column ("created", TypeName = "timestamp with time zone")]
        public DateTime? Created { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }

        [Column ("manufacturer")]
        [StringLength (1042)]
        public string Manufacturer { get; set; }

        [Column ("xid")]
        [StringLength (1042)]
        public string Xid { get; set; }

        [Column ("content_hash")]
        [StringLength (1042)]
        public string ContentHash { get; set; }

        [ForeignKey ("CategoryId")]
        [InverseProperty ("SourceOffer")]
        public Category Category { get; set; }

        [ForeignKey ("ProductId")]
        [InverseProperty ("SourceOffer")]
        public Product Product { get; set; }

        [ForeignKey ("SourceProgramId")]
        [InverseProperty ("SourceOffer")]
        public SourceProgram SourceProgram { get; set; }
    }
}