﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("user_article_like")]
    public partial class UserArticleLike {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("user_id")]
        public int UserId { get; set; }

        [Column ("article_id")]
        public int ArticleId { get; set; }

        [ForeignKey ("ArticleId")]
        [InverseProperty ("UserArticleLike")]
        public Article Article { get; set; }

        [ForeignKey ("UserId")]
        [InverseProperty ("UserArticleLike")]
        public User User { get; set; }
    }
}