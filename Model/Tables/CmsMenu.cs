﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("cms_menu")]
    public partial class CmsMenu {
        public CmsMenu () {
            InverseParent = new HashSet<CmsMenu> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Column ("parent_id")]
        public int? ParentId { get; set; }

        [Column ("cms_page_identify")]
        public int CmsPageIdentify { get; set; }

        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("type")]
        [StringLength (15)]
        public string Type { get; set; }

        [Column ("orderby")]
        public int Orderby { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }

        [ForeignKey ("ParentId")]
        [InverseProperty ("InverseParent")]
        public CmsMenu Parent { get; set; }

        [InverseProperty ("Parent")]
        public ICollection<CmsMenu> InverseParent { get; set; }
    }
}