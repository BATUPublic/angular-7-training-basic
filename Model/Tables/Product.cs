﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("product")]
    public partial class Product {
        public Product () {
            SourceOffer = new HashSet<SourceOffer> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (512)]
        public string Rewrite { get; set; }

        [Column ("category_id")]
        public int CategoryId { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (512)]
        public string Name { get; set; }

        [Required]
        [Column ("description")]
        public string Description { get; set; }

        [Column ("price", TypeName = "numeric(10,2)")]
        public decimal Price { get; set; }

        [Column ("oldprice", TypeName = "numeric(10,2)")]
        public decimal? Oldprice { get; set; }

        [Column ("quantity")]
        public int Quantity { get; set; }

        [Column ("first_photo")]
        public string FirstPhoto { get; set; }

        [Column ("photos_count")]
        public int PhotosCount { get; set; }

        [Column ("url")]
        [StringLength (2048)]
        public string Url { get; set; }

        [Required]
        [Column ("is_available")]
        public bool? IsAvailable { get; set; }

        [Column ("coolness")]
        public int Coolness { get; set; }

        [Column ("shows_count")]
        public int ShowsCount { get; set; }

        [Column ("shop_id")]
        public int? ShopId { get; set; }

        [Required]
        [Column ("visible")]
        public bool? Visible { get; set; }

        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Required]
        [Column ("photos", TypeName = "jsonb")]
        public string Photos { get; set; }

        [Required]
        [Column ("tags", TypeName = "jsonb")]
        public string Tags { get; set; }

        [Required]
        [Column ("labels", TypeName = "jsonb")]
        public string Labels { get; set; }

        [Column ("accept_time", TypeName = "timestamp with time zone")]
        public DateTime AcceptTime { get; set; }

        [Column ("redirect_count")]
        public int RedirectCount { get; set; }

        [Column ("fitured_date", TypeName = "timestamp with time zone")]
        public DateTime? FituredDate { get; set; }

        [Column ("featured", TypeName = "timestamp with time zone")]
        public DateTime? Featured { get; set; }

        [Column ("featured_date", TypeName = "timestamp with time zone")]
        public DateTime? FeaturedDate { get; set; }

        [ForeignKey ("CategoryId")]
        [InverseProperty ("Product")]
        public Category Category { get; set; }

        [InverseProperty ("Product")]
        public ICollection<SourceOffer> SourceOffer { get; set; }
    }
}