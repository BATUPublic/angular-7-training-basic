﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("admin")]
    public partial class Admin {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("username")]
        [StringLength (128)]
        public string Username { get; set; }

        [Column ("password")]
        [StringLength (32)]
        [JsonIgnore]
        public string Password { get; set; }

        [NotMapped]
        public string Repassword { get; set; }

        [Column ("firstname")]
        [StringLength (128)]
        public string Firstname { get; set; }

        [Column ("lastname")]
        [StringLength (256)]
        public string Surname { get; set; }

        [Column ("email")]
        [StringLength (256)]
        public string Email { get; set; }

        [Column ("hash")]
        [StringLength (32)]
        [JsonIgnore]
        public string Hash { get; set; }

        [Column ("avatar")]
        public string Avatar { get; set; }

        [Column ("last_login", TypeName = "timestamp with time zone")]
        public DateTime? LastLogin { get; set; }

        [Column ("group", TypeName = "jsonb")]
        [JsonIgnore]
        public string _groups { get; set; }

        [NotMapped]
        public JArray Groups {
            get {
                return JsonConvert.DeserializeObject<JArray> (string.IsNullOrEmpty (_groups) ? "[]" : _groups);
            }
            set {
                _groups = JsonConvert.SerializeObject (value);
            }
        }

        // [InverseProperty ("From")]
        // public ICollection<Message> SentMessages { get; set; }

        // [InverseProperty ("To")]
        // public ICollection<Message> Messages { get; set; }
    }
}