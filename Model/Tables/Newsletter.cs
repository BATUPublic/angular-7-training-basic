﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("newsletter")]
    public partial class Newsletter {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("email")]
        [StringLength (256)]
        public string Email { get; set; }
    }
}