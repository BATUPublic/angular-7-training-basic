﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("landing_cookbooks")]
    public partial class LandingCookbooks {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("title")]
        [StringLength (256)]
        public string Title { get; set; }

        [Column ("cover", TypeName = "jsonb")]
        public string Cover { get; set; }

        [Column ("publication_date", TypeName = "date")]
        public DateTime? PublicationDate { get; set; }

        [Column ("file", TypeName = "jsonb")]
        public string File { get; set; }

        [Column ("is_accepted")]
        public bool IsAccepted { get; set; }
    }
}