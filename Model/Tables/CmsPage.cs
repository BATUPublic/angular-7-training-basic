﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("cms_page")]
    public partial class CmsPage {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("region")]
        [StringLength (128)]
        public string Region { get; set; }

        [Column ("view")]
        [StringLength (128)]
        public string View { get; set; }

        [Required]
        [Column ("title")]
        [StringLength (128)]
        public string Title { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (128)]
        public string Rewrite { get; set; }

        [Required]
        [Column ("type")]
        [StringLength (15)]
        public string Type { get; set; }

        [Column ("metatitle")]
        [StringLength (128)]
        public string Metatitle { get; set; }

        [Column ("metadescription")]
        [StringLength (256)]
        public string Metadescription { get; set; }

        [Column ("metakeywords")]
        [StringLength (256)]
        public string Metakeywords { get; set; }

        [Column ("require_login")]
        public bool RequireLogin { get; set; }

        [Column ("sections", TypeName = "json")]
        public string Sections { get; set; }

        [Column ("identify")]
        public int Identify { get; set; }

        [Column ("date_from")]
        public DateTime DateFrom { get; set; }

        [Column ("content")]
        public string Content { get; set; }

        [Column ("urlpatterns", TypeName = "jsonb")]
        public string Urlpatterns { get; set; }

        [Column ("master_page_id")]
        public int? MasterPageId { get; set; }

        [Column ("hierarchy_parent_id")]
        public int? HierarchyParentId { get; set; }

        [Column ("breadcrumbs")]
        [StringLength (128)]
        public string Breadcrumbs { get; set; }

        [Required]
        [Column ("metafollow")]
        public bool? Metafollow { get; set; }

        [Required]
        [Column ("metaindex")]
        public bool? Metaindex { get; set; }

        [Column ("og")]
        public string Og { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }
    }
}