﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("param")]
    public partial class Param {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("orderby")]
        public int Orderby { get; set; }

        [Column ("is_sorted")]
        public bool IsSorted { get; set; }

        [Column ("is_filter")]
        public bool IsFilter { get; set; }

        [Column ("is_visible")]
        public bool IsVisible { get; set; }

        [Required]
        [Column ("unique")]
        [StringLength (256)]
        public string Unique { get; set; }

        [Column ("category_id")]
        public int CategoryId { get; set; }

        [Required]
        [Column ("values")]
        public string Values { get; set; }

        [ForeignKey ("CategoryId")]
        [InverseProperty ("Param")]
        public Category Category { get; set; }
    }
}