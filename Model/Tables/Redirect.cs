﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("redirect")]
    public partial class Redirect {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("product_id")]
        public int ProductId { get; set; }

        [Column ("date_time", TypeName = "timestamp with time zone")]
        public DateTime DateTime { get; set; }
    }
}