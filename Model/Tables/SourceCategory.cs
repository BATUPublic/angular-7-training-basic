﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("source_category")]
    public partial class SourceCategory {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (250)]
        public string Name { get; set; }

        [Column ("source_program_id")]
        public int SourceProgramId { get; set; }

        [Column ("target_category_id")]
        public int? TargetCategoryId { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }

        [Column ("import")]
        public bool? Import { get; set; }

        [Column ("hidden")]
        public bool? Hidden { get; set; }

        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Column ("auto_transfer")]
        public bool? AutoTransfer { get; set; }

        [ForeignKey ("SourceProgramId")]
        [InverseProperty ("SourceCategory")]
        public SourceProgram SourceProgram { get; set; }

        [ForeignKey ("TargetCategoryId")]
        [InverseProperty ("SourceCategory")]
        public Category TargetCategory { get; set; }
    }
}