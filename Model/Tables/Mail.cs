﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("mail")]
    public partial class Mail {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("title")]
        [StringLength (128)]
        public string Title { get; set; }

        [Required]
        [Column ("content")]
        public string Content { get; set; }

        [Column ("description")]
        public string Description { get; set; }

        [Required]
        [Column ("type")]
        [StringLength (256)]
        public string Type { get; set; }

        [Column ("attachments")]
        public string Attachments { get; set; }
    }
}