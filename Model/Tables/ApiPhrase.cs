﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("api_phrase")]
    public partial class ApiPhrase {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("phrase")]
        [StringLength (512)]
        public string Phrase { get; set; }

        [Column ("search_in_name")]
        public bool? SearchInName { get; set; }

        [Column ("exists_in_name")]
        public bool? ExistsInName { get; set; }

        [Column ("search_in_description")]
        public bool? SearchInDescription { get; set; }

        [Column ("exists_in_description")]
        public bool? ExistsInDescription { get; set; }

        [Column ("category_id")]
        public int? CategoryId { get; set; }

        [Column ("tags", TypeName = "jsonb")]
        public string Tags { get; set; }

        [ForeignKey ("CategoryId")]
        [InverseProperty ("ApiPhrase")]
        public Category Category { get; set; }
    }
}