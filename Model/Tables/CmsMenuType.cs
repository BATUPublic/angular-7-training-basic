﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("cms_menu_type")]
    public partial class CmsMenuType {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("limit")]
        public int Limit { get; set; }

        [Column ("orderby")]
        public int Orderby { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }
    }
}