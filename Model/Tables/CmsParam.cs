﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("cms_param")]
    public partial class CmsParam {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("rewrite")]
        [StringLength (2048)]
        public string Rewrite { get; set; }

        [Column ("value", TypeName = "json")]
        public string Value { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }
    }
}