﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("author")]
    public partial class Author {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("title")]
        [StringLength (256)]
        public string Title { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (256)]
        public string Rewrite { get; set; }

        [Column ("www")]
        [StringLength (256)]
        public string Www { get; set; }

        [Column ("email")]
        [StringLength (256)]
        public string Email { get; set; }

        [Column ("about")]
        public string About { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [Column ("social")]
        public string Social { get; set; }

        [Column ("sponsor")]
        public string Sponsor { get; set; }

        [Column ("display")]
        public bool Display { get; set; }

        [Column ("firstname")]
        [StringLength (256)]
        public string Firstname { get; set; }

        [Column ("lastname")]
        [StringLength (256)]
        public string Lastname { get; set; }

        [Required]
        [Column ("app")]
        [StringLength (128)]
        public string App { get; set; }

        [Column ("description")]
        public string Description { get; set; }

        [Column ("photo", TypeName = "jsonb")]
        public string Photo { get; set; }

        [Column ("user_id")]
        public int? UserId { get; set; }

        [Column ("password")]
        [StringLength (32)]
        public string Password { get; set; }

        [Column ("video_link")]
        public string VideoLink { get; set; }

        [Column ("logo", TypeName = "jsonb")]
        public string Logo { get; set; }

        [Column ("background_logo", TypeName = "jsonb")]
        public string BackgroundLogo { get; set; }

        [Column ("about_blog_photo", TypeName = "jsonb")]
        public string AboutBlogPhoto { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }

        [Column ("video_title")]
        public string VideoTitle { get; set; }

        [Column ("hash")]
        [StringLength (32)]
        public string Hash { get; set; }
    }
}