﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("advertise")]
    public partial class Advertise {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("place")]
        [StringLength (50)]
        public string Place { get; set; }

        [Column ("category")]
        public string Category { get; set; }

        [Required]
        [Column ("content")]
        public string Content { get; set; }

        [Column ("date_active", TypeName = "date")]
        public DateTime DateActive { get; set; }

        [Column ("active")]
        public bool Active { get; set; }
    }
}