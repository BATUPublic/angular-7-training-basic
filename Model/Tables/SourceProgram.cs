﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("source_program")]
    public partial class SourceProgram {
        public SourceProgram () {
            SourceCategory = new HashSet<SourceCategory> ();
            SourceOffer = new HashSet<SourceOffer> ();
        }

        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("params", TypeName = "jsonb")]
        public string Params { get; set; }

        [Column ("imported_at", TypeName = "timestamp with time zone")]
        public DateTime? ImportedAt { get; set; }

        [Column ("interval")]
        public TimeSpan Interval { get; set; }

        [Column ("raw", TypeName = "jsonb")]
        public string Raw { get; set; }

        [Column ("source_id")]
        public int SourceId { get; set; }

        [Column ("active")]
        public bool Active { get; set; }

        [Column ("affiliation_link")]
        [StringLength (128)]
        public string AffiliationLink { get; set; }

        [Column ("create_at", TypeName = "timestamp with time zone")]
        public DateTime CreateAt { get; set; }

        [Column ("update_at", TypeName = "timestamp with time zone")]
        public DateTime UpdateAt { get; set; }

        [Column ("shop_id")]
        public int? ShopId { get; set; }

        [Column ("last_import_time", TypeName = "timestamp with time zone")]
        public DateTime? LastImportTime { get; set; }

        [Column ("info")]
        public string Info { get; set; }

        [Column ("shop_disabled_date", TypeName = "timestamp with time zone")]
        public DateTime? ShopDisabledDate { get; set; }

        [Column ("shop_disabled")]
        public bool? ShopDisabled { get; set; }

        [ForeignKey ("SourceId")]
        [InverseProperty ("SourceProgram")]
        public Source Source { get; set; }

        [InverseProperty ("SourceProgram")]
        public ICollection<SourceCategory> SourceCategory { get; set; }

        [InverseProperty ("SourceProgram")]
        public ICollection<SourceOffer> SourceOffer { get; set; }
    }
}