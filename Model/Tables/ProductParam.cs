﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("product_param")]
    public partial class ProductParam {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }

        [Required]
        [Column ("type", TypeName = "jsonb")]
        public string Type { get; set; }

        [Column ("is_sorted")]
        public bool IsSorted { get; set; }

        [Column ("is_filter")]
        public bool IsFilter { get; set; }

        [Column ("is_visible")]
        public bool IsVisible { get; set; }

        [Column ("is_required")]
        public bool IsRequired { get; set; }

        [Required]
        [Column ("unique")]
        [StringLength (256)]
        public string Unique { get; set; }

        [Column ("category_id")]
        public int? CategoryId { get; set; }

        [Column ("values", TypeName = "jsonb")]
        public string Values { get; set; }

        [Column ("order_by")]
        public int? OrderBy { get; set; }

        [ForeignKey ("CategoryId")]
        [InverseProperty ("ProductParam")]
        public Category Category { get; set; }
    }
}