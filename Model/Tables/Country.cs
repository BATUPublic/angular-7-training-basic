﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("country")]
    public partial class Country {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("code")]
        [StringLength (2)]
        public string Code { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (256)]
        public string Name { get; set; }
    }
}