﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("patron")]
    public partial class Patron {
        [Column ("id")]
        public int Id { get; set; }

        [Column ("id_firm")]
        public int? IdFirm { get; set; }

        [Column ("patron_name")]
        public string PatronName { get; set; }

        [Column ("section_article")]
        public bool? SectionArticle { get; set; }

        [Column ("section_shop")]
        public bool? SectionShop { get; set; }

        [Column ("category", TypeName = "jsonb")]
        public string Category { get; set; }

        [Column ("photo", TypeName = "jsonb")]
        public string Photo { get; set; }

        [Column ("images", TypeName = "jsonb")]
        public string Images { get; set; }
    }
}