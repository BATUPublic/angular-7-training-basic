﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BATUSystems.AdminPanel.Model.Tables {
    [Table ("article_category")]
    public partial class ArticleCategory {
        [Column ("id")]
        public int Id { get; set; }

        [Required]
        [Column ("name")]
        [StringLength (128)]
        public string Name { get; set; }

        [Required]
        [Column ("rewrite")]
        [StringLength (128)]
        public string Rewrite { get; set; }

        [Column ("parent_id")]
        public int? ParentId { get; set; }

        [Column ("orderby")]
        public int? Orderby { get; set; }

        [Column ("description")]
        public string Description { get; set; }

        [Column ("isvisible")]
        public bool? Isvisible { get; set; }

        [Column ("images")]
        public string Images { get; set; }

        [ForeignKey ("ParentId")]
        [InverseProperty ("ArticleCategory")]
        public Category Parent { get; set; }
    }
}