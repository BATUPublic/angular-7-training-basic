using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BATUSystems.AdminPanel.API.Services {
    public enum SortDirection {
        DESC,
        ASC
    }

    public interface IAdminService {
        Task<Admin> Authenticate (string username, string password);
        Task<int> Count ();
        Task<IEnumerable<Admin>> Get (string filter = "", string sortColumn = "id", SortDirection sortDirection = SortDirection.ASC, int pageIndex = 0, int pageSize = 10);
        Task<Admin> One (int id);
        int GetNextId ();
        Admin Create (Admin admin);
        Task<Admin> Update (int id, Admin _admin);
        Task<Admin> Delete (int id);
    }

    public class AdminService : IAdminService {
        private readonly BakalieDevContext context;
        private readonly DbSet<Admin> table;
        public AdminService (BakalieDevContext _context) {
            context = _context;
            table = context.Admin;
        }

        private string CreateMD5 (string input) {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create ()) {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes (input);
                byte[] hashBytes = md5.ComputeHash (inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder ();
                for (int i = 0; i < hashBytes.Length; i++) {
                    sb.Append (hashBytes[i].ToString ("X2"));
                }
                return sb.ToString ().ToLower ();
            }
        }

        public async Task<Admin> Authenticate (string username, string password) {
            var admin = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Username == username)
            );

            if (admin == null) {
                return null;
            }

            var md5 = CreateMD5 (admin.Id.ToString () + password);

            if (admin.Password != md5) {
                return null;
            }

            return admin;
        }

        public async Task<int> Count () {
            var count = await Task.Run (
                () => table.Count ()
            );

            return count;
        }

        public async Task<IEnumerable<Admin>> Get (string filter = "", string sortColumn = "id", SortDirection sortDirection = SortDirection.ASC, int pageIndex = 0, int pageSize = 10) {
            var admins = await Task.Run (
                () => {
                    var q = table.AsQueryable ();

                    if (sortDirection == SortDirection.ASC) {
                        switch (sortColumn.ToLower ()) {
                            case "id":
                                q = q.OrderBy (x => x.Id);
                                break;
                            case "firstname":
                                q = q.OrderBy (x => x.Firstname);
                                break;
                            case "surname":
                                q = q.OrderBy (x => x.Surname);
                                break;
                            case "username":
                                q = q.OrderBy (x => x.Username);
                                break;
                            case "email":
                                q = q.OrderBy (x => x.Email);
                                break;
                        }

                    } else {
                        switch (sortColumn.ToLower ()) {
                            case "id":
                                q = q.OrderByDescending (x => x.Id);
                                break;
                            case "firstname":
                                q = q.OrderByDescending (x => x.Firstname);
                                break;
                            case "surname":
                                q = q.OrderByDescending (x => x.Surname);
                                break;
                            case "username":
                                q = q.OrderByDescending (x => x.Username);
                                break;
                            case "email":
                                q = q.OrderByDescending (x => x.Email);
                                break;
                        }
                    }

                    if (filter.Length > 0) {
                        q = q.Where (x =>
                            Regex.IsMatch (x.Email, filter, RegexOptions.IgnoreCase) ||
                            Regex.IsMatch (x.Username, filter, RegexOptions.IgnoreCase) ||
                            Regex.IsMatch (x.Firstname, filter, RegexOptions.IgnoreCase) ||
                            Regex.IsMatch (x.Surname, filter, RegexOptions.IgnoreCase)
                        );
                    }

                    return q
                        .Skip (pageIndex * pageSize)
                        .Take (pageSize)
                        .ToList ();
                }
            );

            return admins;
        }

        public async Task<Admin> One (int id) {
            var admin = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Id == id)
            );

            if (admin == null) {
                return null;
            }

            return admin;
        }

        public int GetNextId () {
            int adminId = -1;
            using (DbCommand cmd = context.Database.GetDbConnection ().CreateCommand ()) {
                cmd.CommandText = "select nextval('admin_id_seq')::int";
                context.Database.OpenConnection ();
                DbDataReader reader = cmd.ExecuteReader ();
                reader.Read ();
                adminId = (int) reader.GetValue (0);
                context.Database.CloseConnection ();
            }
            return adminId;
        }

        public Admin Create (Admin admin) {
            admin.Password = CreateMD5 (admin.Id.ToString () + admin.Password);

            context.Admin.Add (admin);
            context.SaveChanges ();
            return admin;
        }

        public async Task<Admin> Update (int id, Admin _admin) {
            var admin = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Id == id)
            );

            if (admin == null) {
                return null;
            }

            if (_admin.Repassword != null && _admin.Repassword.Length > 0) {
                admin.Password = CreateMD5 (_admin.Id.ToString () + _admin.Repassword);
            }

            admin.Avatar = _admin.Avatar;
            admin.Username = _admin.Username;
            admin.Email = _admin.Email;
            admin.Firstname = _admin.Firstname;
            admin.Surname = _admin.Surname;
            admin.Groups = _admin.Groups;

            context.Admin.Update (admin);
            context.SaveChanges ();

            return admin;
        }

        public async Task<Admin> Delete (int id) {
            var admin = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Id == id)
            );

            if (admin == null) {
                return null;
            }

            table.Remove (admin);
            context.SaveChanges ();

            return admin;
        }
    }
}