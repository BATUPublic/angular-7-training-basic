using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BATUSystems.AdminPanel.API.Services {
    public interface IMessageService {
        Task<IEnumerable<Message>> Get (int offset = 0, int limit = 50);
        Task<Message> One (int id);
        Task<Message> Update (int id, Message _item);
        Task<Message> MarkAsRead (int id);
        Task<int> Count (MessageType[] filter);
    }

    public enum MessageType {
        ALL,
        READ,
        UNREAD,
        IMPORTANT,
    }

    public class MessageService : IMessageService {
        private readonly BakalieDevContext context;
        private readonly DbSet<Message> table;
        public MessageService (BakalieDevContext _context) {
            context = _context;
            table = context.Message;
        }

        public async Task<int> Count (MessageType[] filter) {
            var count = await Task.Run (
                () => {
                    var q = table.AsQueryable ();

                    if (filter.Contains (MessageType.UNREAD)) {
                        q = q.Where (x => x.Read == null);
                    }
                    if (filter.Contains (MessageType.READ)) {
                        q = q.Where (x => x.Read != null);
                    }
                    if (filter.Contains (MessageType.IMPORTANT)) {
                        q = q.Where (x => x.Important == true);
                    }

                    return q.Count ();
                }
            );

            return count;
        }

        public async Task<IEnumerable<Message>> Get (int offset = 0, int limit = 50) {
            var messages = await Task.Run (
                () => table
                .Include (_message => _message.From)
                .Include (_message => _message.To)
                .OrderByDescending (_message => _message.Sent)
                .Skip (offset)
                .Take (limit)
                .ToList ()
            );

            return messages;
        }

        public async Task<Message> One (int id) {
            var message = await Task.Run (
                () => table
                .Include (_message => _message.From)
                .Include (_message => _message.To)
                .SingleOrDefault (x => x.Id == id)
            );

            if (message == null) {
                return null;
            }

            return message;
        }

        public async Task<Message> Update (int id, Message _message) {
            var message = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Id == id)
            );

            if (message == null) {
                return null;
            }

            table.Update (message);
            context.SaveChanges ();

            return message;
        }

        public async Task<Message> MarkAsRead (int id) {
            var message = await Task.Run (
                () => table
                .SingleOrDefault (x => x.Id == id)
            );

            if (message == null) {
                return null;
            }

            message.Read = DateTime.Now;

            table.Update (message);
            context.SaveChanges ();

            return message;
        }
    }
}