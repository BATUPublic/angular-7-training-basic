using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace BATUSystems.AdminPanel.API.Helpers {
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions> {
        private readonly IAdminService _adminService;

        public BasicAuthenticationHandler (
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IAdminService adminService) : base (options, logger, encoder, clock) => _adminService = adminService;

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync () {

            // Console.WriteLine ("protected override async Task<AuthenticateResult> HandleAuthenticateAsync ()");

            if (!Request.Headers.ContainsKey ("Authorization") && !Request.Query.ContainsKey ("token"))
                return AuthenticateResult.Fail ("Missing Authorization Header");

            Admin admin = null;
            try {
                byte[] credentialBytes;
                if (Request.Headers.ContainsKey ("Authorization")) {
                    var authHeader = AuthenticationHeaderValue.Parse (Request.Headers["Authorization"]);
                    credentialBytes = Convert.FromBase64String (authHeader.Parameter);
                } else {
                    credentialBytes = Convert.FromBase64String (Request.Query["token"]);
                }

                var credentials = Encoding.UTF8.GetString (credentialBytes).Split (':');
                var adminname = credentials[0];
                var password = credentials[1];
                admin = await _adminService.Authenticate (adminname, password);
            } catch {
                return AuthenticateResult.Fail ("Invalid Authorization Header");
            }

            if (admin == null)
                return AuthenticateResult.Fail ("Invalid Username or Password");

            var claims = new List<Claim> {
                new Claim (ClaimTypes.NameIdentifier, admin.Id.ToString ()),
                new Claim (ClaimTypes.Name, admin.Username),
                new Claim (ClaimTypes.Surname, admin.Surname),
                new Claim (ClaimTypes.GivenName, admin.Firstname),
                new Claim (ClaimTypes.Role, "User")
            };

            var identity = new ClaimsIdentity (claims, Scheme.Name);
            foreach(var group in admin.Groups) {
                identity.AddClaim(new Claim (ClaimTypes.Role, group.ToString()));
            }
            
            var principal = new ClaimsPrincipal (identity);
            var ticket = new AuthenticationTicket (principal, Scheme.Name);

            return AuthenticateResult.Success (ticket);
        }
    }
}