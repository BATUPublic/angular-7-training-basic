﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BATUSystems.AdminPanel.API.Controllers {
    [Authorize]
    [Route ("api/message")]
    [ApiController]
    public class MessageController : ControllerBase {
        private readonly IMessageService service;

        public MessageController (IMessageService _service) => service = _service;

        // GET api/admin
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Message>>> Get (int offset = 0, int limit = 50) {
            var items = await service.Get (offset, limit);

            if (items == null)
                return BadRequest ();

            return Ok (items);
        }

        // GET api/admin/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<Message>> One (int id) {
            var item = await service.One (id);

            if (item == null)
                return BadRequest (new { message = "Id is incorrect" });

            return Ok (item);
        }

        // POST api/admin
        [HttpPost]
        public void Post ([FromBody] string value) { }

        // PUT api/admin/5
        [HttpPut ("{id}")]
        public async Task<IActionResult> Update (int id, [FromBody] Message _item) {
            var item = await service.Update (id, _item);

            if (item == null)
                return BadRequest (new { message = "Id is incorrect" });

            return NoContent ();
        }

        // PUT api/admin/5/read
        [HttpPut ("{id}/read")]
        public async Task<IActionResult> MarkAsRead (int id) {
            var item = await service.MarkAsRead (id);

            if (item == null)
                return BadRequest (new { message = "Id is incorrect" });

            return NoContent ();
        }

        // DELETE api/admin/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }

        [HttpGet ("count")]
        public async Task<ActionResult<int>> Count (MessageType[] filter) {
            var items = await service.Count (filter);
            return Ok (new { value = items });

        }
    }
}