﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace BATUSystems.AdminPanel.API.Controllers {
    // [Authorize(Roles = "Admin")]
    [Route ("api/admin")]
    [ApiController]
    public class AdminController : ControllerBase {
        private readonly IAdminService service;
        private readonly string avatarPath;

        public AdminController (IAdminService _service, IHostingEnvironment _hostEnvironment) {
            service = _service;
            avatarPath = Path.Combine (_hostEnvironment.WebRootPath, "img", "avatars");
        }

        [AllowAnonymous]
        [HttpPost ("authenticate")]
        public async Task<IActionResult> Authenticate ([FromBody] AuthAdmin _item) {
            var item = await service.Authenticate (_item.Username, _item.Password);

            if (item == null)
                return BadRequest (new {
                    message = "Username or password is incorrect"
                });

            return Ok (item);
        }

        // GET api/admin
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Admin>>> Get (string filter = "", string sortColumn = "id", SortDirection sortDirection = SortDirection.ASC, int pageIndex = 0, int pageSize = 10) {
            var items = await service.Get (filter, sortColumn, sortDirection, pageIndex, pageSize);

            // await ChatWebSocketMiddleware.SendStringToAll ("Ktoś właśnie odczytał liste administratorw");
            // await ChatWebSocketMiddleware.SendObjectToAll (items.First ());

            if (items == null)
                return BadRequest ();

            return Ok (items);
        }

        // GET api/admin/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<Admin>> One (int id) {
            var item = await service.One (id);

            if (item == null)
                return BadRequest (new { message = "Id is incorrect" });

            return Ok (item);
        }

        // POST api/admin
        [HttpPost]
        public ActionResult<Admin> Create ([FromBody] Admin _item) {
            _item.Id = service.GetNextId ();

            if (_item.Avatar.Contains ("data:image")) {
                SaveAvatar (_item);
            }

            var item = service.Create (_item);

            if (item == null)
                return NotFound (new { message = "Id is incorrect" });

            return Ok (item);
        }
        // PUT api/admin/5
        [HttpPut ("{id}")]
        public async Task<IActionResult> Update (int id, [FromBody] Admin _item) {
            if (_item.Avatar.Contains ("data:image")) {
                SaveAvatar (_item);
            }

            var item = await service.Update (id, _item);

            if (item == null)
                return NotFound (new { message = "Id is incorrect" });

            return NoContent ();
        }

        // DELETE api/admin/5
        [HttpDelete ("{id}")]
        public async Task<IActionResult> Delete (int id) {
            var item = await service.Delete (id);

            if (item == null)
                return NotFound (new { message = "Id is incorrect" });

            return NoContent ();

        }

        [HttpGet ("count")]
        public async Task<ActionResult<int>> Count () {
            var items = await service.Count ();
            return Ok (new { value = items });
        }

        private void SaveAvatar (Admin _item) {
            int id = _item.Id;
            string filePath = Path.Combine (avatarPath, "_" + id.ToString () + ".jpg");
            var base64 = _item.Avatar.Split (',') [1];
            var bytes = Convert.FromBase64String (base64);
            using (var imageFile = new FileStream (filePath, FileMode.Create)) {
                imageFile.Write (bytes, 0, bytes.Length);
                imageFile.Flush ();
            }
            _item.Avatar = Path.Combine ("img", "avatars", "_" + id.ToString () + ".jpg");
        }
    }

    public class AuthAdmin {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}