\babel@toc {english}{}
\beamer@sectionintoc {1}{Wst\IeC {\k e}p}{4}{1}{1}
\beamer@subsectionintoc {1}{1}{Przydatne linki}{4}{1}{1}
\beamer@subsectionintoc {1}{2}{JavaScript vs. ECMAScript vs. TypeScript}{5}{1}{1}
\beamer@subsectionintoc {1}{3}{AngularJs vs. Angular}{8}{1}{1}
\beamer@sectionintoc {2}{\IeC {\'S}rodowisko pracy}{10}{1}{2}
\beamer@subsectionintoc {2}{1}{Instalacja .Net Core, NodeJS i klienta git}{10}{1}{2}
\beamer@subsectionintoc {2}{2}{Instalacja TypeScript i @angular/cli}{11}{1}{2}
\beamer@sectionintoc {3}{Podstawy TypeScript}{12}{1}{3}
\beamer@subsectionintoc {3}{1}{Typy}{12}{1}{3}
\beamer@subsectionintoc {3}{2}{Objektowo\IeC {\'s}\IeC {\'c}}{14}{1}{3}
\beamer@subsectionintoc {3}{3}{Enkapsulacja}{20}{1}{3}
\beamer@subsectionintoc {3}{4}{Atrybuty}{21}{1}{3}
\beamer@sectionintoc {4}{Podstawy Angular 7}{22}{1}{4}
\beamer@subsectionintoc {4}{1}{Inicjalizacja aplikacji}{22}{1}{4}
\beamer@subsectionintoc {4}{2}{@ngModule}{23}{1}{4}
\beamer@subsectionintoc {4}{3}{@Component}{25}{1}{4}
\beamer@subsectionintoc {4}{4}{@Directive}{27}{1}{4}
\beamer@subsectionintoc {4}{5}{@Injectable}{29}{1}{4}
\beamer@subsectionintoc {4}{6}{Renderowanie i linkowanie}{30}{1}{4}
\beamer@subsectionintoc {4}{7}{Instalacja bibliotek}{33}{1}{4}
\beamer@sectionintoc {5}{Budowa aplikacji}{35}{2}{5}
\beamer@subsectionintoc {5}{1}{Template projektu}{35}{2}{5}
\beamer@subsectionintoc {5}{2}{Komponenty statyczne}{38}{2}{5}
\beamer@subsectionintoc {5}{3}{Komponent Dashboard}{40}{2}{5}
\beamer@subsectionintoc {5}{4}{Komponent AdminList}{43}{2}{5}
