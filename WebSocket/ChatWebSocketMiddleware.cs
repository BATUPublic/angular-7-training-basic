using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using BATUSystems.AdminPanel.WebSockets.Model;
using BATUSystems.AdminPanel.WebSockets.Models;
using BATUSystems.AdminPanel.WebSockets.Types;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BATUSystems.AdminPanel.WebSockets {

    public class UserSocket {
        public string userId { get; set; }
        public WebSocket socket { get; set; }
    }
    public class ChatWebSocketMiddleware {
        private static ConcurrentDictionary<string, UserSocket> _sockets = new ConcurrentDictionary<string, UserSocket> ();

        private readonly RequestDelegate _next;
        private IMapper _mapper;

        public ChatWebSocketMiddleware (RequestDelegate next) {
            _next = next;
        }

        private async Task OnConnect (string userId, WebSocket socket, BakalieDevContext dbContext) {
            var admins = dbContext.Admin
                .Where (a => a.Id.ToString () != userId)
                .ToList ();

            var friends = _mapper.Map<IList<Admin>, IList<ChatFriend>> (admins);

            await Send (socket, new ChatTransport () { Type = ChatTransportType.UserList, Content = friends });
        }

        private async Task OnMessage (string message, CancellationToken ct) {

            foreach (var userSocket in _sockets) {
                if (userSocket.Value.socket.State != WebSocketState.Open) {
                    continue;
                }

                await Send (userSocket.Value.socket, message, ct);
            }
        }

        public async Task Invoke (HttpContext httpContext, BakalieDevContext dbContext, IMapper mapper) {

            if (!httpContext.WebSockets.IsWebSocketRequest) {
                await _next.Invoke (httpContext);
                return;
            }

            AuthenticateResult authenticateResult = await httpContext.AuthenticateAsync ("BasicAuthentication");

            if (!authenticateResult.Succeeded) {
                await _next.Invoke (httpContext);
                return;
            }

            _mapper = mapper;

            CancellationToken ct = httpContext.RequestAborted;
            WebSocket currentSocket = await httpContext.WebSockets.AcceptWebSocketAsync ();

            var userId = authenticateResult.Principal.Claims.FirstOrDefault (c => c.Type == ClaimTypes.NameIdentifier).Value;
            var socketId = Guid.NewGuid ().ToString ();

            if (_sockets.TryAdd (socketId, new UserSocket { userId = userId, socket = currentSocket })) {
                await OnConnect (userId, currentSocket, dbContext);
            }

            while (true) {
                if (ct.IsCancellationRequested) {
                    break;
                }

                var response = await ReceiveStringAsync (currentSocket, ct);
                if (string.IsNullOrEmpty (response)) {
                    if (currentSocket.State != WebSocketState.Open) {
                        break;
                    }
                    continue;
                }

                await OnMessage (response, ct);
            }

            UserSocket dummy;
            _sockets.TryRemove (socketId, out dummy);

            if (currentSocket.State == WebSocketState.Open || currentSocket.State == WebSocketState.CloseReceived || currentSocket.State == WebSocketState.CloseSent) {
                await currentSocket.CloseAsync (WebSocketCloseStatus.NormalClosure, "Closing", ct);
            }
            currentSocket.Dispose ();
        }

        public static async Task SendToAll (string value) {
            foreach (var socket in _sockets) {
                if (socket.Value.socket.State != WebSocketState.Open) {
                    continue;
                }

                await Send (socket.Value.socket, value);
            }
        }

        public static async Task Send (WebSocket socket, object value) {
            if (socket.State != WebSocketState.Open) {
                return;
            }
            var json = JsonConvert.SerializeObject (value, Formatting.Indented, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                    ContractResolver = new LowercaseContractResolver ()
            });

            await Send (socket, json);
        }

        public static async Task SendToAll (object value) {
            var json = JsonConvert.SerializeObject (value, Formatting.Indented, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                    ContractResolver = new LowercaseContractResolver ()
            });

            foreach (var socket in _sockets) {
                if (socket.Value.socket.State != WebSocketState.Open) {
                    continue;
                }

                await Send (socket.Value.socket, json);
            }
        }

        private static Task Send (WebSocket socket, string data, CancellationToken ct = default (CancellationToken)) {
            var buffer = Encoding.UTF8.GetBytes (data);
            var segment = new ArraySegment<byte> (buffer);
            return socket.SendAsync (segment, WebSocketMessageType.Text, true, ct);
        }

        private static async Task<string> ReceiveStringAsync (WebSocket socket, CancellationToken ct = default (CancellationToken)) {
            var buffer = new ArraySegment<byte> (new byte[8192]);
            using (var ms = new MemoryStream ()) {
                WebSocketReceiveResult result;
                do {
                    ct.ThrowIfCancellationRequested ();

                    result = await socket.ReceiveAsync (buffer, ct);
                    ms.Write (buffer.Array, buffer.Offset, result.Count);
                }
                while (!result.EndOfMessage);

                ms.Seek (0, SeekOrigin.Begin);
                if (result.MessageType != WebSocketMessageType.Text) {
                    return null;
                }

                // Encoding UTF8: https://tools.ietf.org/html/rfc6455#section-5.6
                using (var reader = new StreamReader (ms, Encoding.UTF8)) {
                    return await reader.ReadToEndAsync ();
                }
            }
        }
    }
}