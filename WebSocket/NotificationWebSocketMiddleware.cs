using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BATUSystems.AdminPanel.WebSockets {
    public class NotificationWebSocketMiddleware {
        private static ConcurrentDictionary<string, WebSocket> _sockets = new ConcurrentDictionary<string, WebSocket> ();

        private readonly RequestDelegate _next;

        public NotificationWebSocketMiddleware (RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke (HttpContext context) {

            if (!context.WebSockets.IsWebSocketRequest) {
                await _next.Invoke (context);
                return;
            }

            CancellationToken ct = context.RequestAborted;
            WebSocket currentSocket = await context.WebSockets.AcceptWebSocketAsync ();
            var socketId = Guid.NewGuid ().ToString ();

            AuthenticateResult authenticateResult = await context.AuthenticateAsync ("BasicAuthentication");

            if (!authenticateResult.Succeeded) {
                await _next.Invoke (context);
                return;
            }

            var claim1 = authenticateResult.Principal.Claims.FirstOrDefault (c => c.Type == ClaimTypes.Name);

            Console.WriteLine ("Notification User: " + claim1.Value);

            _sockets.TryAdd (socketId, currentSocket);

            while (true) {
                if (ct.IsCancellationRequested) {
                    break;
                }

                var response = await ReceiveStringAsync (currentSocket, ct);
                if (string.IsNullOrEmpty (response)) {
                    if (currentSocket.State != WebSocketState.Open) {
                        break;
                    }

                    continue;
                }

                foreach (var socket in _sockets) {
                    if (socket.Value.State != WebSocketState.Open) {
                        continue;
                    }

                    await SendStringAsync (socket.Value, "pong: " + response, ct);
                }
            }

            WebSocket dummy;
            _sockets.TryRemove (socketId, out dummy);

            if (currentSocket.State == WebSocketState.Open ||
                currentSocket.State == WebSocketState.CloseReceived ||
                currentSocket.State == WebSocketState.CloseSent) {
                await currentSocket.CloseAsync (WebSocketCloseStatus.NormalClosure, "Closing", ct);
            }
            currentSocket.Dispose ();
        }

        public static async Task SendStringToAll (string value) {
            foreach (var socket in _sockets) {
                if (socket.Value.State != WebSocketState.Open) {
                    continue;
                }

                await SendStringAsync (socket.Value, value);
            }
        }
        public static async Task SendObjectToAll (object value) {
            foreach (var socket in _sockets) {
                if (socket.Value.State != WebSocketState.Open) {
                    continue;
                }

                await SendStringAsync (socket.Value, JsonConvert.SerializeObject (value, Formatting.Indented, new JsonSerializerSettings {
                    TypeNameHandling = TypeNameHandling.Objects,
                        TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                        ContractResolver = new LowercaseContractResolver ()
                }));
            }
        }

        private static Task SendStringAsync (WebSocket socket, string data, CancellationToken ct = default (CancellationToken)) {
            var buffer = Encoding.UTF8.GetBytes (data);
            var segment = new ArraySegment<byte> (buffer);
            return socket.SendAsync (segment, WebSocketMessageType.Text, true, ct);
        }

        private static async Task<string> ReceiveStringAsync (WebSocket socket, CancellationToken ct = default (CancellationToken)) {
            var buffer = new ArraySegment<byte> (new byte[8192]);
            using (var ms = new MemoryStream ()) {
                WebSocketReceiveResult result;
                do {
                    ct.ThrowIfCancellationRequested ();

                    result = await socket.ReceiveAsync (buffer, ct);
                    ms.Write (buffer.Array, buffer.Offset, result.Count);
                }
                while (!result.EndOfMessage);

                ms.Seek (0, SeekOrigin.Begin);
                if (result.MessageType != WebSocketMessageType.Text) {
                    return null;
                }

                // Encoding UTF8: https://tools.ietf.org/html/rfc6455#section-5.6
                using (var reader = new StreamReader (ms, Encoding.UTF8)) {
                    return await reader.ReadToEndAsync ();
                }
            }
        }
    }
}