using Newtonsoft.Json.Serialization;

namespace BATUSystems.AdminPanel.WebSockets {

    public class LowercaseContractResolver : DefaultContractResolver {
        protected override string ResolvePropertyName (string propertyName) {
            return propertyName.ToLower ();
        }
    }
}