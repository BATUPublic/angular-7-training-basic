using AutoMapper;
using BATUSystems.AdminPanel.Model.Tables;
using BATUSystems.AdminPanel.WebSockets.Model;

namespace BATUSystems.AdminPanel.WebSockets {
    public class ChatProfile : Profile {
        public ChatProfile () {
            CreateMap<Admin, ChatFriend> ()
                .ForMember (destination => destination.Status, source => source.Ignore ());
        }
    }
}