using System;
using BATUSystems.AdminPanel.Model.Tables;

namespace BATUSystems.AdminPanel.WebSockets.Types {

    public enum ChatTransportType {
        UserList,
        User
    }
}