using System;
using BATUSystems.AdminPanel.WebSockets.Types;

namespace BATUSystems.AdminPanel.WebSockets.Models {
    public class ChatTransport {
        public ChatTransportType Type { get; set; }
        public object Content { get; set; }
    }
}