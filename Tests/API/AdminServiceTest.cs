using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace BATUSystems.AdminPanel.Tests.API {
    public class AdminServiceTest {

        private BakalieDevContext _context;

        private AdminService PrepareAdminService () {
            // Arrange
            var fixture = new Fixture ();

            var admins = new List<Admin> {
                fixture.Build<Admin> ().With (a => a.Id, 2).With (a => a.Username, "barcis").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 4).With (a => a.Username, "adam").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 3).With (a => a.Username, "ola").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 1).With (a => a.Username, "admin").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 22).With (a => a.Username, "barcis2").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 24).With (a => a.Username, "adam2").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 23).With (a => a.Username, "ola2").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 21).With (a => a.Username, "admin2").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 32).With (a => a.Username, "barcis3").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 34).With (a => a.Username, "adam3").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 33).With (a => a.Username, "ola3").Create (),
                fixture.Build<Admin> ().With (a => a.Id, 31).With (a => a.Username, "admin3").Create ()
            }.AsQueryable ();

            var adminsMock = new Mock<DbSet<Admin>> ();
            adminsMock.As<IQueryable<Admin>> ().Setup (m => m.Provider).Returns (admins.Provider);
            adminsMock.As<IQueryable<Admin>> ().Setup (m => m.Expression).Returns (admins.Expression);
            adminsMock.As<IQueryable<Admin>> ().Setup (m => m.ElementType).Returns (admins.ElementType);
            adminsMock.As<IQueryable<Admin>> ().Setup (m => m.GetEnumerator ()).Returns (admins.GetEnumerator ());

            var dbContextMock = new Mock<BakalieDevContext> ();
            dbContextMock.Setup (x => x.Admin).Returns (adminsMock.Object);

            _context = dbContextMock.Object;

            return new AdminService (_context);
        }

        [Fact]
        public async Task Get () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.Get ();

            // Assert
            Assert.Equal (_context.Admin.OrderBy (x => x.Id).AsEnumerable ().First ().Id, results.First ().Id);
        }

        [Fact]
        public async Task Get_outOrder () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.Get ();

            // Assert
            Assert.Equal (_context.Admin.OrderBy (x => x.Id).First (), results.First ());
        }

        [Fact]
        public async Task Get_OrderByName () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.Get ("", "surname", SortDirection.ASC);

            // Assert
            Assert.Equal (_context.Admin.OrderBy (x => x.Surname).First (), results.First ());
        }

        [Fact]
        public async Task Get_Limit3Page0 () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.Get ("", "id", SortDirection.ASC, 0, 3);

            // Assert
            Assert.Equal (3, results.Count ());
        }

        [Fact]
        public async Task Get_Limit3Page1 () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.Get ("", "id", SortDirection.ASC, 1, 3);

            // Assert
            Assert.Equal (3, results.Count ());
            Assert.Equal (_context.Admin.First (x => x.Id == 4), results.First ());
        }

        [Fact]
        public async Task One () {
            var adminService = PrepareAdminService ();
            // Act
            var results = await adminService.One (1);

            // Assert
            Assert.Equal (_context.Admin.First (x => x.Id == 1), results);
        }

        [Fact]
        public void Create () {
            var fixture = new Fixture ();
            var adminService = PrepareAdminService ();

            var newAdmin = fixture.Build<Admin> ().With (a => a.Id, 2000).With (a => a.Username, "nowy").With (a => a.Password, "testpassword").Create ();

            var countBeforeAdd = _context.Admin.Count ();

            // Act
            var results = adminService.Create (newAdmin);

            // Assert
            Assert.Equal ("49e910dcfb53400a75aa61f1857d5032", results.Password);
            Assert.Equal (2000, results.Id);
            Assert.Equal (countBeforeAdd + 1, _context.Admin.Count ());
            // Assert.Equal (_context.Admin.First(x => x.Id == 2000).Id, results.Id);
        }
    }
}