using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using BATUSystems.AdminPanel.API.Services;
using BATUSystems.AdminPanel.Model;
using BATUSystems.AdminPanel.Model.Tables;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace BATUSystems.AdminPanel.Tests.API {
    public class MessageServiceTest {

        private IQueryable<Message> messages;

        private MessageService PrepareMessageService () {
            // Arrange
            var fixture = new Fixture ();

            messages = new List<Message> {
                fixture.Build<Message> ().With (a => a.Id, 2).With (a => a.Topic, "Druga wiadomość").Create (),
                fixture.Build<Message> ().With (a => a.Id, 3).With (a => a.Topic, "Trzecia wiadomość").Create (),
                fixture.Build<Message> ().With (a => a.Id, 4).With (a => a.Topic, "Czwarta wiadomość").Create (),
                fixture.Build<Message> ().With (a => a.Id, 5).With (a => a.Topic, "Piąta wiadomość").Create (),
                fixture.Build<Message> ().With (a => a.Id, 1).With (a => a.Topic, "Pierwsza wiadomość").Create (),
                
            }.AsQueryable ();

            var messagesMock = new Mock<DbSet<Message>> ();
            messagesMock.As<IQueryable<Message>> ().Setup (m => m.Provider).Returns (messages.Provider);
            messagesMock.As<IQueryable<Message>> ().Setup (m => m.Expression).Returns (messages.Expression);
            messagesMock.As<IQueryable<Message>> ().Setup (m => m.ElementType).Returns (messages.ElementType);
            messagesMock.As<IQueryable<Message>> ().Setup (m => m.GetEnumerator ()).Returns (messages.GetEnumerator ());

            var dbContextMock = new Mock<BakalieDevContext> ();
            dbContextMock.Setup (x => x.Message).Returns (messagesMock.Object);

            return new MessageService (dbContextMock.Object);
        }

        // [Fact]
        // public async Task Get () {
        //     MessageService messageService = PrepareMessageService();
        //     // Act
        //     var results = await messageService.Get ();

        //     // Assert
        //     Assert.Equal (messages.OrderBy(x => x.Id).AsEnumerable (), results);
        // }

        // [Fact]
        // public async Task Get_WithoutOrder () {
        //     MessageService MessageService = PrepareMessageService();
        //     // Act
        //     var results = await MessageService.Get ();

        //     // Assert
        //     Assert.Equal (messages.OrderBy(x => x.Id).First(), results.First());
        // }

        // [Fact]
        // public async Task Get_WithOrderByName () {
        //     MessageService MessageService = PrepareMessageService();
        //     // Act
        //     var results = await MessageService.Get ("", "surname", SortDirection.ASC);

        //     // Assert
        //     Assert.Equal (messages.OrderBy(x => x.Surname).First(), results.First());
        // }

        // [Fact]
        // public async Task One () {
        //     MessageService MessageService = PrepareMessageService();
        //     // Act
        //     var results = await MessageService.One (1);

        //     // Assert
        //     Assert.Equal (messages.First(x => x.Id == 1), results);
        // }
    }
}