# Szkolenie: **Podstawy Angular 7**

## Przydatne linki
- [Visual Studio Code](https://code.visualstudio.com/download "Edytor od firmy Microsoft") (https://code.visualstudio.com/download)
- [.NET Core 2.2](https://dotnet.microsoft.com/download "Framework dla aplikacji internetowych") (https://dotnet.microsoft.com/download)
- [NodeJS i NPM](https://nodejs.org/en/download/ "Biblioteki i menadżer pakietów") (https://nodejs.org/en/download/)
- [Git](https://gitforwindows.org/ "Konsolowy klient git dla Windows") (https://gitforwindows.org/)
- [GitKraken](https://www.gitkraken.com/download "Graficzny klient git") (https://www.gitkraken.com/download)

## Komendy

1. Klonowanie źródła projektu za pomocą GIT
```bash
git clone https://gitlab.com/BATUPublic/angular-7-training-basic.git AdminPanel
```
2. Inicjalizacja aplikacji
```bash
cd Panel
ng new AdminPanel
# ? Would you like to add Angular routing? Yes
# ? Which stylesheet format would you like to use? LESS
```
3. Kompilacja i uruchomienie aplikacji
```bash
cd AdminPanel
# ng build
ng serve
```

## Początkowa zawartość plików:
1. /src/app/app.module.ts
```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent       // deklaracja komponentu glownego
  ],
  imports: [
    BrowserModule,     // import modulu przegladarki
    AppRoutingModule   // import routingu aplikacji
  ],
  providers: [],
  bootstrap: [
    AppComponent       // ustawienie komponentu glownego
  ]
})
export class AppModule { }
```
2. /src/app/app.app-routing.module.ts
```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
3. /src/app/app.component.ts
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'AdminPanel';
}
```
3. /src/app/app.component.html
```html
<!-- The content below is only a placeholder and can be replaced. -->
<div style="text-align:center">
  <h1>Welcome to {{ title }}!</h1>
  <img
    width="300"
    alt="Angular Logo"
    src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg=="
  />
</div>
<h2>Here are some links to help you start:</h2>
<ul>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://angular.io/tutorial">Tour of Heroes</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://angular.io/cli">CLI Documentation</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://blog.angular.io/">Angular blog</a></h2>
  </li>
</ul>

<router-outlet></router-outlet>

```
